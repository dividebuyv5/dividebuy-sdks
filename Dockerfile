# syntax=docker/dockerfile:experimental
FROM composer:1.9 as build
WORKDIR /app
RUN mkdir -p -m 0600 ~/.ssh && ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts
COPY composer.json composer.json
RUN --mount=type=ssh,id=bitbucket composer install \
                                              --ignore-platform-reqs \
                                              --no-interaction \
                                              --optimize-autoloader \
                                              --no-scripts \
                                              --prefer-dist

# Set master image
FROM php:7.4-fpm-alpine

ARG BUILD_ENV=local
ENV BUILD_ENV ${BUILD_ENV:-production}
RUN if [ "$BUILD_ENV" = "production" ] ; then mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini" ; fi
RUN apk --no-cache add $PHPIZE_DEPS --virtual .build-deps && pecl channel-update pecl.php.net \
 && pecl install pcov \
 && docker-php-ext-enable pcov \
 && echo "pcov.enabled=1" >> $PHP_INI_DIR/php.ini \
 && apk del --no-network .build-deps

WORKDIR /app
# Copy existing application directory permissions
COPY --chown=www-data:www-data ./.php_cs.dist /app/.php_cs.dist
COPY --chown=www-data:www-data ./phpunit.xml.dist /app/phpunit.xml.dist
COPY --chown=www-data:www-data ./composer.json /app/composer.json
COPY --chown=www-data:www-data ./src /app/src
COPY --chown=www-data:www-data ./tests /app/tests
COPY --chown=www-data:www-data --from=build /app/vendor/ /app/vendor/
COPY --from=build /app/vendor/ /app/vendor/

# Expose port 9000 and start php-fpm server
EXPOSE 9000
CMD ["php-fpm"]
