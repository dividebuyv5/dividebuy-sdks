<?php

declare(strict_types=1);

namespace DivideBuySdk\Constant;

final class Endpoints
{
  public const URL_CHECK_ACCESS = '/checkaccess';
  public const URL_CONFIRM_RETAILER = '/confirmretailer';
  public const URL_REFUND = '/refund';
  public const URL_UPDATE_RETAILER_STATUS = '/updateretailerstatus';
  public const URL_USER_ORDER = '/getuserorder';
  public const URL_TRACKING = '/tracking';
  public const URL_SYNC_RETAILER_ORDER = '/syncretorder';
  public const URL_GET_SOFT_SEARCH_KEY = '/get-softsearch-key';
  public const URL_VERIFY_TOKEN_PERMISSION = '/verify-token-permission';
  public const URL_ACTIVATE_POS = '/activatepossystem';
  public const URL_CREATE_CUSTOM_POS_ORDER = '/createcustomposorder';
}
