<?php

declare(strict_types=1);

namespace DivideBuySdk\Constant;

final class IncomingRequestParameters
{
  public const IN_STORE_TOKEN = 'store_token';
  public const IN_STORE_AUTH = 'store_authentication';
  public const IN_STORE_ORDER_ID = 'store_order_id';
  public const IN_ORDER_STATUS = 'order_status';
  public const IN_PHONE_ORDER_ENABLED = 'is_phone_order_enabled';
  public const IN_ADDRESS = 'address';
  public const IN_FIRST_NAME = 'first_name';
  public const IN_LAST_NAME = 'last_name';
  public const IN_ORDER_TIME = 'orderTime';
  public const IN_ORDER_TOTAL = 'order_total';
  public const IN_CUSTOMER_EMAIL = 'customer_email';
  public const IN_ORDER_REF_ID = 'laravel_order_ref_id';
  public const IN_METHOD = 'method';
  public const IN_RETAILER_ID = 'retailer_id';
  public const IN_USER_POST_CODE = 'user_postcode';
  public const IN_ORDER_ID = 'order_id';
  public const IN_RETAILER_STORE_CODE = 'retailer_store_code';
  public const IN_RETAILER_CONFIG_DETAILS = 'retailerConfigurationDetails';
  public const IN_COURIERS = 'couriers';
  public const IN_ALLOWED_IPS = 'allowedIps';
  public const ACTIVATE_POS = 'activate_pos';
  public const STORE_CODE = 'store_code';
}
