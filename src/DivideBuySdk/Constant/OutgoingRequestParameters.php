<?php

declare(strict_types=1);

namespace DivideBuySdk\Constant;

final class OutgoingRequestParameters
{
  public const OUT_STORE_TOKEN = 'storeToken';
  public const OUT_STORE_AUTH = 'storeAuthentication';
}
