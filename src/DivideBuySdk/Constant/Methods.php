<?php

declare(strict_types=1);

namespace DivideBuySdk\Constant;

final class Methods
{
  public const RETAILER_CONFIGURATIONS = 'retailerConfigurations';
  public const GET_ORDER_DETAILS = 'getOrderDetails';
  public const VERIFY_POSTCODE = 'verifyPostcode';
  public const ORDER_SUCCESS = 'orderSuccess';
  public const DELETE_ORDER = 'orderDelete';
  public const CANCEL_ORDER = 'orderCancel';
  public const UPDATE_COURIERS = 'updateCouriers';
}
