<?php

declare(strict_types=1);

namespace DivideBuySdk;

use DivideBuySdk\Request\Incoming\IncomingRequestInterface;
use DivideBuySdk\Request\Outgoing\OutgoingRequestInterface;
use DivideBuySdk\Service\DomainResolver;
use Psr\Log\LoggerInterface;

interface ApiInterface
{
  public function syncRetailerOrder(int $storeOrderId): array;

  public function trackOrder(array $parameters): array;

  public function checkAccess(): array;

  public function getSoftSearchKey(array $postParams): array;

  public function verifyTokenPermission(array $postParams): array;

  public function confirmRetailer(array $postParams): array;

  public function updateRetailerStatus(array $postParams): array;

  public function getUserOrder(string $storeOrderId): array;

  public function refundOrder(array $postParams): array;

  public function retailerConfiguration(callable $callbackFunction): array;

  public function cancelOrder(callable $callbackFunction): array;

  public function successOrder(callable $callbackFunction): array;

  public function deleteOrder(callable $callbackFunction): array;

  public function getOrderDetails(callable $callbackFunction): array;

  public function fetchCouriers(callable $callbackFunction): array;

  public function verifyPostcode(callable $callbackFunction): array;

  public function updateCouriers(callable $callbackFunction): array;

  public function activatePosSystem(callable $callbackFunction): array;

  public function createCustomPosOrder(callable $callbackFunction): array;

  public function setRequestSource(string $requestSource): self;

  public function getUrlResolver(): DomainResolver;

  public function getIncomingRequest(): IncomingRequestInterface;

  public function getOutgoingRequest(): OutgoingRequestInterface;

  public function changeEnvironment(string $environment);

  public function isStaging(): bool;

  public function isProduction(): bool;

  public function isLocal(): bool;

  public function setLogger(LoggerInterface $logger);

  public function getLogger(): LoggerInterface;

  public function getIncomingRequestMethod(): string;
}
