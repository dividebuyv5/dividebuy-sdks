<?php

declare(strict_types=1);

namespace DivideBuySdk;

use DivideBuySdk\Api\IncomingApi;
use DivideBuySdk\Api\OutgoingApi;
use DivideBuySdk\Constant\Endpoints;
use DivideBuySdk\Request\Incoming\IncomingRequestInterface;
use DivideBuySdk\Request\Outgoing\OutgoingRequestInterface;
use DivideBuySdk\Service\DomainResolver;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;

final class Api implements LoggerAwareInterface, ApiInterface
{
  use LoggerAwareTrait;

  protected IncomingApi $incomingApi;
  protected OutgoingApi $outgoingApi;
  protected DomainResolver $domainResolver;

  public function __construct(
      IncomingApi $incoming,
      OutgoingApi $outgoing,
      DomainResolver $domainResolver,
      LoggerInterface $logger
  ) {
    $this->incomingApi = $incoming;
    $this->outgoingApi = $outgoing;
    $this->domainResolver = $domainResolver;
    $this->logger = $logger;
  }

  /**
   * @param  array  $postParams
   *
   * @return mixed
   *
   * @throws GuzzleException
   */
  public function verifyTokenPermission(array $postParams): array
  {
    return $this->outgoingApi->sendRequest(Endpoints::URL_VERIFY_TOKEN_PERMISSION, [], $postParams);
  }

  /**
   * @param  array  $postParams
   *
   * @return mixed
   *
   * @throws GuzzleException
   */
  public function getSoftSearchKey(array $postParams): array
  {
    return $this->outgoingApi->sendRequest(Endpoints::URL_GET_SOFT_SEARCH_KEY, [], $postParams);
  }

  /**
   * @param  int  $storeOrderId
   *
   * @return mixed
   *
   * @throws GuzzleException
   */
  public function syncRetailerOrder(int $storeOrderId): array
  {
    $postParams = ['storeOrderId' => $storeOrderId];

    return $this->outgoingApi->sendRequest(Endpoints::URL_SYNC_RETAILER_ORDER, [], $postParams);
  }

  /**
   * @param  array  $parameters
   *
   * @return array
   *
   * @throws GuzzleException
   */
  public function trackOrder(array $parameters): array
  {
    return $this->outgoingApi->sendRequest(Endpoints::URL_TRACKING, [], $parameters);
  }

  /**
   * @return array
   *
   * @throws GuzzleException
   */
  public function checkAccess(): array
  {
    return $this->outgoingApi->sendRequest(Endpoints::URL_CHECK_ACCESS);
  }

  /**
   * @param array $postParams
   *
   * @return array
   *
   * @throws GuzzleException
   */
  public function confirmRetailer(array $postParams): array
  {
    return $this->outgoingApi->sendRequest(Endpoints::URL_CONFIRM_RETAILER, [], $postParams);
  }

  /**
   * @param array $postParams
   *
   * @return array
   *
   * @throws GuzzleException
   */
  public function updateRetailerStatus(array $postParams): array
  {
    return $this->outgoingApi->sendRequest(Endpoints::URL_UPDATE_RETAILER_STATUS, [], $postParams);
  }

  /**
   * @param string $storeOrderId
   *
   * @return array
   *
   * @throws GuzzleException
   */
  public function getUserOrder(string $storeOrderId): array
  {
    $postParams = ['storeOrderId' => $storeOrderId];

    return $this->outgoingApi->sendRequest(Endpoints::URL_USER_ORDER, [], $postParams);
  }

  /**
   * @param array $postParams
   *
   * @return array
   *
   * @throws GuzzleException
   */
  public function refundOrder(array $postParams): array
  {
    return $this->outgoingApi->sendRequest(Endpoints::URL_REFUND, [], $postParams);
  }

  public function retailerConfiguration(callable $callbackFunction): array
  {
    return $this->incomingApi->processIncomingRequest($callbackFunction);
  }

  public function cancelOrder(callable $callbackFunction): array
  {
    return $this->incomingApi->processIncomingRequest($callbackFunction);
  }

  public function createCustomPosOrder(callable $callbackFunction): array
  {
    return $this->incomingApi->processCreateCustomPosOrder($callbackFunction);
  }

  public function activatePosSystem(callable $callbackFunction): array
  {
    return $this->incomingApi->processActivatePos($callbackFunction);
  }

  public function successOrder(callable $callbackFunction): array
  {
    return $this->incomingApi->processIncomingRequest($callbackFunction);
  }

  public function deleteOrder(callable $callbackFunction): array
  {
    return $this->incomingApi->processIncomingRequest($callbackFunction);
  }

  public function getOrderDetails(callable $callbackFunction): array
  {
    return $this->incomingApi->processGetOrderDetails($callbackFunction);
  }

  public function fetchCouriers(callable $callbackFunction): array
  {
    return $this->incomingApi->processFetchCouriers($callbackFunction);
  }

  public function verifyPostcode(callable $callbackFunction): array
  {
    return $this->incomingApi->processVerifyPostcode($callbackFunction);
  }

  public function updateCouriers(callable $callbackFunction): array
  {
    return $this->incomingApi->processIncomingRequest($callbackFunction);
  }

  public function setRequestSource(string $requestSource): self
  {
    $this->incomingApi->setRequestSource($requestSource);

    return $this;
  }

  public function getUrlResolver(): DomainResolver
  {
    return $this->domainResolver;
  }

  public function getIncomingRequest(): IncomingRequestInterface
  {
    return $this->incomingApi->getRequest();
  }

  public function getOutgoingRequest(): OutgoingRequestInterface
  {
    return $this->outgoingApi->getRequest();
  }

  public function changeEnvironment(string $environment)
  {
    $this->domainResolver->setEnvironment($environment);
    $this->logger->info(__METHOD__ . 'Application environment changed to: ' . $environment);
  }

  public function isStaging(): bool
  {
    return $this->domainResolver->getEnvironment() === DomainResolver::ENV_STAGING;
  }

  public function isProduction(): bool
  {
    return $this->domainResolver->getEnvironment() === DomainResolver::ENV_PRODUCTION;
  }

  public function isLocal(): bool
  {
    return ! $this->isProduction() && ! $this->isStaging();
  }

  public function setLogger(LoggerInterface $logger)
  {
    $this->logger = $logger;
    $this->outgoingApi->setLogger($logger);
    $this->incomingApi->setLogger($logger);
  }

  public function getLogger(): LoggerInterface
  {
    return $this->logger;
  }

  public function getIncomingRequestMethod(): string
  {
    return $this->incomingApi->getRequestMethod();
  }
}
