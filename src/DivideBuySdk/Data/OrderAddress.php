<?php

declare(strict_types=1);

namespace DivideBuySdk\Data;

class OrderAddress extends AbstractData
{
  public function getStreet(): string
  {
    return (string) @$this->street;
  }

  public function getPostCode(): string
  {
    return (string) @$this->postcode;
  }

  public function getRegion(): string
  {
    return (string) @$this->region;
  }

  public function getCity(): string
  {
    return (string) @$this->city;
  }

  public function getHouseNumber(): string
  {
    return (string) @$this->house_number;
  }

  public function getHouseName(): string
  {
    return (string) @$this->house_name;
  }

  public function getTelephone(): string
  {
    return (string) @$this->telephone;
  }

  public function getContactNumber(): string
  {
    return (string) @$this->contact_number;
  }

  public function getEmail(): string
  {
    return (string) @$this->email;
  }

  public function getPrefix(): string
  {
    return (string) @$this->prefix;
  }

  public function getAddress2(): string
  {
    return (string) @$this->address2;
  }
}
