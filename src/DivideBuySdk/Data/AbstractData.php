<?php

declare(strict_types=1);

namespace DivideBuySdk\Data;

use ArrayObject;

abstract class AbstractData extends ArrayObject
{
  public function __construct(array $option)
  {
    parent::__construct($option, ArrayObject::ARRAY_AS_PROPS);
  }
}
