<?php

declare(strict_types=1);

namespace DivideBuySdk\Service\Authentication;

use DivideBuySdk\Constant\OutgoingRequestParameters;
use DivideBuySdk\Exception\InvalidParameterException;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\ScopeInterface;
use Dividebuy\Common\Utility\StoreConfigHelper;
use Magento\Framework\App\ObjectManager;

class Parameters
{
  private string $storeToken;
  private string $storeAuthentication;
  private ?StoreManagerInterface $storeManager = null; // Initialize as null to avoid uninitialized access
  private ?ScopeConfigInterface $scopeConfig = null;   // Initialize as null (if required later)

  public function __construct(string $storeToken, string $storeAuthentication)
  {
    $this->storeAuthentication = $storeAuthentication;
    $this->storeToken = $storeToken;
  }

  public function getAuthParameters(): array
  {
    $this->isValid();

    return [
        OutgoingRequestParameters::OUT_STORE_TOKEN => $this->getStoreToken(),
        OutgoingRequestParameters::OUT_STORE_AUTH => $this->getStoreAuthentication(),
    ];
  }

  // Get store code (checking if storeManager is set)
  public function getStoreCode(): string
{
    if (!$this->storeManager) {
        // Using ObjectManager to retrieve StoreManagerInterface
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->storeManager = $objectManager->get(\Magento\Store\Model\StoreManagerInterface::class);
    }

    try {
        // If you're passing store code as a parameter, set the store explicitly
        $storeCodeFromRequest = $this->getStoreCodeFromRequest();
        if ($storeCodeFromRequest) {
            $this->storeManager->setCurrentStore($storeCodeFromRequest);
        }

        // Get the current store
        $currentStore = $this->storeManager->getStore();
        $storeCode = $currentStore->getCode();
    } catch (\Exception $e) {
        // In case of any exception, return a default store code
        $storeCode = 'default';
    }

    return $storeCode;
}

// Helper method to get the store code from the request or any other source
private function getStoreCodeFromRequest(): ?string
{
    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    $request = $objectManager->get(\Magento\Framework\App\RequestInterface::class);
    return $request->getParam('store', null);
}

  public function isValid(): bool
  {
    if (empty($this->storeAuthentication)) {
      throw new InvalidParameterException('Store Authentication not found', 400);
    }
    if (empty($this->storeToken)) {
      throw new InvalidParameterException('Store Token not found', 400);
    }

    return true;
  }

  public function getStoreToken(): string
  {
    $objectManager = ObjectManager::getInstance();
    $storeConfigHelper = $objectManager->get(StoreConfigHelper::class);
    $this->storeToken = $storeConfigHelper->getStoreToken($this->getStoreCode());
    return $this->storeToken;
  }

  public function getStoreAuthentication(): string
  {
    $objectManager = ObjectManager::getInstance();
    $storeConfigHelper = $objectManager->get(StoreConfigHelper::class);
    $this->storeAuthentication = $storeConfigHelper->getAuthenticationKey($this->getStoreCode());
    return $this->storeAuthentication;
  }
}
