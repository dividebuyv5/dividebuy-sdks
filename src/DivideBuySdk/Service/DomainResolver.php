<?php

declare(strict_types=1);

namespace DivideBuySdk\Service;

use DivideBuySdk\Helper\ArrayUtilityTrait;
use Psr\Log\LoggerInterface;

class DomainResolver
{
  use ArrayUtilityTrait;

  public const ENV_STAGING = 'staging';
  public const ENV_PRODUCTION = 'production';
  public const ENV_LOCAL = 'development';

  public const DOMAIN_API = 'DIVIDE_BUY_DOMAIN_API';
  public const DOMAIN_ORDER = 'DIVIDE_BUY_DOMAIN_ORDER';
  public const DOMAIN_PORTAL = 'DIVIDE_BUY_DOMAIN_PORTAL';
  public const DOMAIN_SOFT_SEARCH = 'DIVIDE_BUY_DOMAIN_SOFT_SEARCH';

  private static array $domainOptions = [
      self::ENV_STAGING => [
          self::DOMAIN_API => 'https://api.dividebuysandbox.co.uk/',
          self::DOMAIN_ORDER => 'dividebuysandbox.co.uk/#/login',
          self::DOMAIN_PORTAL => 'https://portal.dividebuysandbox.co.uk/?adminToken=',
          self::DOMAIN_SOFT_SEARCH => 'https://softsearch.dividebuysandbox.co.uk',
      ],
      self::ENV_PRODUCTION => [
          self::DOMAIN_API => 'https://api.dividebuy.co.uk/',
          self::DOMAIN_ORDER => 'dividebuy.co.uk/#/login',
          self::DOMAIN_PORTAL => 'https://portal.dividebuy.co.uk/?adminToken=',
          self::DOMAIN_SOFT_SEARCH => 'https://softsearch.dividebuy.co.uk',
      ],
      self::ENV_LOCAL => [
          self::DOMAIN_API => 'https://api.dividebuydemo.co.uk/',
          self::DOMAIN_ORDER => 'dividebuydemo.co.uk/#/login',
          self::DOMAIN_PORTAL => 'https://portal.dividebuydemo.co.uk/?adminToken=',
          self::DOMAIN_SOFT_SEARCH => 'https://softsearch.dividebuydemo.co.uk',
      ],
  ];

  private string $apiDomain;

  private string $orderDomain;

  private string $portalDomain;

  private string $softSearchDomain;

  private LoggerInterface $logger;

  private string $environment = self::ENV_PRODUCTION;

  public function __construct(LoggerInterface $logger, ?string $environment = '')
  {
    $this->logger = $logger;
    $this->setEnvironment($environment);
  }

  public function getApiDomain(): string
  {
    return $this->apiDomain;
  }

  public function getOrderDomain(): string
  {
    return $this->orderDomain;
  }

  public function getPortalDomain(): string
  {
    return $this->portalDomain;
  }

  public function getSoftSearchDomain(): string
  {
    return $this->softSearchDomain;
  }

  public function setEnvironment(?string $environment = ''): self
  {
    if ($environment) {
      $this->environment = $environment;
    }
    $this->initDomains();

    return $this;
  }

  public function getEnvironment(): string
  {
    return $this->environment;
  }

  /**
   * @return array<array<\string>>
   */
  public static function getDomainOptions(): array
  {
    return self::$domainOptions;
  }

  private function initDomains()
  {
    switch ($this->environment) {
            case self::ENV_PRODUCTION:
                $this->setDomains(self::$domainOptions[self::ENV_PRODUCTION]);
                break;
            case self::ENV_STAGING:
                $this->setDomains(self::$domainOptions[self::ENV_STAGING]);
                break;
            case self::ENV_LOCAL:
                $this->setDomains(self::$domainOptions[self::ENV_LOCAL]);
                break;
    }
  }

  private function setDomains(array $params): void
  {
    $this->softSearchDomain = $this->get($params, self::DOMAIN_SOFT_SEARCH);
    $this->apiDomain = $this->get($params, self::DOMAIN_API);
    $this->portalDomain = $this->get($params, self::DOMAIN_PORTAL);
    $this->orderDomain = $this->get($params, self::DOMAIN_ORDER);
  }

  private function setLocalEnv(): void
  {
    $this->softSearchDomain = $this->getEnv(self::DOMAIN_SOFT_SEARCH);
    $this->apiDomain = $this->getEnv(self::DOMAIN_API);
    $this->portalDomain = $this->getEnv(self::DOMAIN_PORTAL);
    $this->orderDomain = $this->getEnv(self::DOMAIN_ORDER);
  }
}
