<?php

declare(strict_types=1);

namespace DivideBuySdk\Response;

use Psr\Http\Message\ResponseInterface as GuzzleResponse;
use Throwable;

interface ResponseInterface
{
  public function setResult(GuzzleResponse $result): ResponseInterface;

  public function getResult(): GuzzleResponse;

  public function toArray(): array;

  public function getSuccessPayload(): array;

  public function getFailurePayload(Throwable $error): array;
}
