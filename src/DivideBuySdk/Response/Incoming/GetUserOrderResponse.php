<?php

declare(strict_types=1);

namespace DivideBuySdk\Response\Incoming;

use DivideBuySdk\Response\CommonResponse;

class GetUserOrderResponse extends CommonResponse implements IncomingResponseInterface
{
  public function toArray(): array
  {
    $content = json_decode((string) $this->result->getBody(), true);
    if (isset($content['status']) and $content['status'] === 'ok') {
      return json_decode($content['data'], true);
    }

    return $content;
  }
}
