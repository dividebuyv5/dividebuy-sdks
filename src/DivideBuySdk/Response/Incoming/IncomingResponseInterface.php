<?php

declare(strict_types=1);

namespace DivideBuySdk\Response\Incoming;

use DivideBuySdk\Response\ResponseInterface;

interface IncomingResponseInterface extends ResponseInterface
{
}
