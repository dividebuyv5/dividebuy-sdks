<?php

declare(strict_types=1);

namespace DivideBuySdk\Response\Outgoing;

use DivideBuySdk\Response\CommonResponse;

class CancelOrderResponse extends CommonResponse implements OutgoingResponseInterface
{
  public function getSuccessPayload(): array
  {
    return [
        'error' => 0,
        'success' => 1,
        'status' => 'ok',
        'order_id' => $this->payload->store_order_id,
    ];
  }
}
