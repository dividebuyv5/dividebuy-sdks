<?php

declare(strict_types=1);

namespace DivideBuySdk\Response\Outgoing;

use DivideBuySdk\Response\ResponseInterface;

interface OutgoingResponseInterface extends ResponseInterface
{
}
