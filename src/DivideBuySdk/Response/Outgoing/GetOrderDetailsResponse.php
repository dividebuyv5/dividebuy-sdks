<?php

declare(strict_types=1);

namespace DivideBuySdk\Response\Outgoing;

use DivideBuySdk\Helper\ArrayUtilityTrait;
use DivideBuySdk\Response\CommonResponse;

class GetOrderDetailsResponse extends CommonResponse implements OutgoingResponseInterface
{
  use ArrayUtilityTrait;

  private array $orderDetails;

  private array $rootFields = [
      'order_detail',
      'product_details',
      'shipping_address',
      'billing_address',
  ];

  private array $orderFields = [
      'store_order_id',
      'store_order_increment_id',
      'store_token',
      'store_authentication',
      'grand_total',
      'subtotal',
      'subtotal_incl_vat',
      'shipping',
      'shipping_incl_vat',
      'shipping_label',
        //'shipping_method', //observed field is not present in Magento2
        //'vat', // observed fields is not present in Magento2
  ];

  private array $productFields = [
      'name',
      'sku',
      'qty',
      'price',
      'price_incl_vat',
      'row_total',
      'row_total_incl_vat',
      'short_description',
      'product_visibility',
      'div_vat',
      'image_url',
  ];

  private array $addressFields = [
      'first_name',
      'last_name',
        // 'email', //Email address not always present
      'street',
      'postcode',
      'region',
      'city',
  ];

  public function setOrderDetails(array $orderDetails): self
  {
    $this->orderDetails = $this->normaliseKeys($orderDetails);

    return $this;
  }

  public function toArray(): array
  {
    $this->validateResponse();

    return $this->orderDetails;
  }

  private function validateResponse(): bool
  {
    $this->validateRootFields();

    $this->validateOrderDetail();
    $this->validateProducts();
    $this->validateShippingAddress();
    $this->validateBillingAddress();

    return true;
  }

  private function validateRootFields(): bool
  {
    return $this->validate($this->rootFields);
  }

  private function validateOrderDetail(): bool
  {
    return $this->validate($this->orderFields, 'order_detail');
  }

  private function validateProducts(): bool
  {
    $total = count($this->orderDetails['product_details']);
    for ($index = 0; $index < $total; $index++) {
      $this->validate($this->productFields, 'product_details.' . $index);
    }

    return true;
  }

  private function validateShippingAddress(): bool
  {
    return $this->validate($this->addressFields, 'shipping_address');
  }

  private function validateBillingAddress(): bool
  {
    return $this->validate($this->addressFields, 'billing_address');
  }

  private function validate(array $requiredFields, ?string $section = null): bool
  {
    $data = empty($section) ? $this->orderDetails : $this->get($this->orderDetails, $section, []);
    foreach ($requiredFields as $field) {
      if (array_key_exists($field, $data) && is_numeric($data[$field])) {
        continue;
      }
      // commented out pending decision on ticket https://dividebuy.atlassian.net/browse/EX-146
            /*if (empty($data[$field])) {
               $message = trim("$section: $field is required", ': ');
               throw new MissingParameterException($message, 400);
           }*/
    }

    return true;
  }
}
