<?php

declare(strict_types=1);

namespace DivideBuySdk\Response\Outgoing;

use DivideBuySdk\Response\CommonResponse;

class CustomPosOrderResponse extends CommonResponse implements OutgoingResponseInterface
{
  public function getSuccessPayload(): array
  {
    $data = parent::getSuccessPayload();

    return array_merge($data, $this->toArray());
  }
}
