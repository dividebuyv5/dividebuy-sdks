<?php

declare(strict_types=1);

namespace DivideBuySdk\Response\Outgoing;

use DivideBuySdk\Response\CommonResponse;

class VerifyPostcodeResponse extends CommonResponse implements OutgoingResponseInterface
{
  public function getSuccessPayload(): array
  {
    return [
        'status' => '200',
        'error' => 0,
        'success' => 1,
        'message' => 'ok',
    ];
  }
}
