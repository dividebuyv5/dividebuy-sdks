<?php

declare(strict_types=1);

namespace DivideBuySdk\Response\Outgoing;

use DivideBuySdk\Response\CommonResponse;

class OrderSuccessResponse extends CommonResponse implements OutgoingResponseInterface
{
  public function getSuccessPayload(): array
  {
    return [
        'status' => 'ok',
        'order_id' => $this->payload->store_order_id,
        'message' => 'Order placed successfully',
    ];
  }
}
