<?php

declare(strict_types=1);

namespace DivideBuySdk\Response;

use DivideBuySdk\Data\AbstractData;
use Psr\Http\Message\ResponseInterface as GuzzleResponse;
use Throwable;

class CommonResponse implements ResponseInterface
{
  protected GuzzleResponse $result;

  protected ?AbstractData $payload;

  public function __construct(?AbstractData $requestPayload = null)
  {
    $this->payload = $requestPayload;
  }

  public function setResult(GuzzleResponse $result): ResponseInterface
  {
    $this->result = $result;

    return $this;
  }

  public function toArray(): array
  {
    if ($this->payload) {
      return $this->payload->getArrayCopy();
    }

    $body = (string) $this->result->getBody();

    if ($this->result->getStatusCode() >= 500) {
      return [
          'error' => 1,
          'success' => 0,
          'status' => $this->result->getStatusCode(),
          'message' => $body,
      ];
    }

    return json_decode($body, true);
  }

  public function getSuccessPayload(): array
  {
    return [
        'error' => 0,
        'success' => 1,
        'status' => 'ok',
    ];
  }

  public function getFailurePayload(Throwable $error): array
  {
    return [
        'error' => 1,
        'success' => 0,
        'message' => $error->getMessage(),
        'status' => (string) $error->getCode(),
    ];
  }

  public function getResult(): GuzzleResponse
  {
    return $this->result;
  }
}
