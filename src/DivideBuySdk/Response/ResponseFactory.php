<?php

declare(strict_types=1);

namespace DivideBuySdk\Response;

use DivideBuySdk\Constant\Endpoints;
use DivideBuySdk\Constant\Methods;
use DivideBuySdk\Data\AbstractData;
use DivideBuySdk\Request\Outgoing\OutgoingRequestInterface;
use DivideBuySdk\Request\RequestInterface;
use DivideBuySdk\Response\Incoming\GetUserOrderResponse;
use DivideBuySdk\Response\Incoming\IncomingResponseInterface;
use DivideBuySdk\Response\Outgoing\CancelOrderResponse;
use DivideBuySdk\Response\Outgoing\CustomPosOrderResponse;
use DivideBuySdk\Response\Outgoing\GetOrderDetailsResponse;
use DivideBuySdk\Response\Outgoing\OrderSuccessResponse;
use DivideBuySdk\Response\Outgoing\VerifyPostcodeResponse;

class ResponseFactory
{
  public function create(?RequestInterface $request = null, ?string $method = null): ResponseInterface
  {
    if ($request instanceof OutgoingRequestInterface) {
      return $this->getIncomingResponse($request);
    }

    $method = $method ? $method : $request->getMethod();

    switch ($method) {
      case Methods::VERIFY_POSTCODE:
        return new VerifyPostcodeResponse($request->getContent());
            case Methods::CANCEL_ORDER:
                return new CancelOrderResponse($request->getContent());
            case Methods::ORDER_SUCCESS:
                return new OrderSuccessResponse($request->getContent());
            case Methods::GET_ORDER_DETAILS:
                return new GetOrderDetailsResponse($request->getContent());
      default:
        return $this->createCommonResponse();
    }
  }

  public function createCommonResponse(?AbstractData $payload = null): CommonResponse
  {
    return new CommonResponse($payload);
  }

  public function getOutgoingResponse(string $endPoint, AbstractData $payload): ResponseInterface
  {
    switch ($endPoint) {
      case Endpoints::URL_CREATE_CUSTOM_POS_ORDER:
        return new CustomPosOrderResponse($payload);
      default:
        return $this->createCommonResponse($payload);
    }
  }

  /**
   * @param  OutgoingRequestInterface  $request
   *
   * @return ResponseInterface|IncomingResponseInterface
   */
  private function getIncomingResponse(OutgoingRequestInterface $request): ResponseInterface
  {
    $endPoint = $request->getEndpoint();

    switch ($endPoint) {
            case Endpoints::URL_USER_ORDER:
                return new GetUserOrderResponse();
            default:
                return $this->createCommonResponse();
    }
  }
}
