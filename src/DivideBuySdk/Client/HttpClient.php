<?php

declare(strict_types=1);

namespace DivideBuySdk\Client;

use DivideBuySdk\Request\Outgoing\OutgoingRequestInterface;
use DivideBuySdk\Response\ResponseInterface;
use DivideBuySdk\Service\Authentication\Parameters;
use DivideBuySdk\Service\DomainResolver;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;

final class HttpClient
{
  private Parameters $parameters;

  private ClientInterface $client;

  private DomainResolver $urlResolver;

  public function __construct(ClientInterface $client, Parameters $authParameters, DomainResolver $urlResolver)
  {
    $this->parameters = $authParameters;
    $this->client = $client;
    $this->urlResolver = $urlResolver;
  }

  /**
   * @param OutgoingRequestInterface $request
   * @param ResponseInterface $response
   *
   * @return array
   *
   * @throws GuzzleException
   */
  public function send(OutgoingRequestInterface $request, ResponseInterface $response): array
  {
    $result = $this->client->request(
        $request->getMethod(),
        $this->prepareUrl($request->getEndpoint()),
        [
            'query' => $request->getQueryParams(),
            'json' => array_merge(
                $request->getPostParams(),
                $this->parameters->getAuthParameters()
            ),
        ]
    );

    return $response->setResult($result)->toArray();
  }

  public function getUrlResolver(): DomainResolver
  {
    return $this->urlResolver;
  }

  private function prepareUrl(string $endpoint): string
  {
    return rtrim($this->urlResolver->getApiDomain(), '/') . '/api/' . ltrim($endpoint, '/');
  }
}
