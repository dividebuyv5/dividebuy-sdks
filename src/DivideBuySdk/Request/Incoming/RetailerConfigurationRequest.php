<?php

declare(strict_types=1);

namespace DivideBuySdk\Request\Incoming;

use DivideBuySdk\Constant\IncomingRequestParameters;
use DivideBuySdk\Data\ConfigOption;

class RetailerConfigurationRequest extends AbstractRequest
{
  public function getRetailerId()
  {
    return $this->getParameterValue(IncomingRequestParameters::IN_RETAILER_ID);
  }

  /**
   * @return array<ConfigOption>
   */
  public function getConfigurationDetails(): array
  {
    return array_map(
        static function ($item) {
          return new ConfigOption($item);
        },
        $this->getParameterValue(IncomingRequestParameters::IN_RETAILER_CONFIG_DETAILS, [])
    );
  }
}
