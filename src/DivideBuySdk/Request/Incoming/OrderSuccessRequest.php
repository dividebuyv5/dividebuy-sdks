<?php

declare(strict_types=1);

namespace DivideBuySdk\Request\Incoming;

use DateTime;
use DivideBuySdk\Constant\IncomingRequestParameters;
use DivideBuySdk\Data\OrderAddress;
use Exception;

class OrderSuccessRequest extends AbstractRequest
{
  public function getStoreOrderId(): int
  {
    return (int) $this->getParameterValue(IncomingRequestParameters::IN_STORE_ORDER_ID);
  }

  public function getOrderStatus(): string
  {
    return (string) $this->getParameterValue(IncomingRequestParameters::IN_ORDER_STATUS);
  }

  public function isPhoneOrderEnabled(): bool
  {
    return filter_var(
        $this->getParameterValue(IncomingRequestParameters::IN_PHONE_ORDER_ENABLED),
        FILTER_VALIDATE_BOOL
    );
  }

  public function getAddress(): OrderAddress
  {
    $address = $this->getParameterValue(IncomingRequestParameters::IN_ADDRESS);

    return new OrderAddress($address);
  }

  /**
   * @return DateTime
   *
   * @throws Exception
   */
  public function getOrderTime(): DateTime
  {
    $date = $this->getParameterValue(IncomingRequestParameters::IN_ORDER_TIME);

    return new DateTime($date);
  }

  public function getCustomerEmail(): string
  {
    return (string) $this->getParameterValue(IncomingRequestParameters::IN_CUSTOMER_EMAIL);
  }

  public function getOrderReferenceId(): string
  {
    return (string) $this->getParameterValue(IncomingRequestParameters::IN_ORDER_REF_ID);
  }
}
