<?php

declare(strict_types=1);

namespace DivideBuySdk\Request\Incoming;

use DivideBuySdk\Request\RequestInterface;

interface IncomingRequestInterface extends RequestInterface
{
  public function validateCredentials(string $tokenFromConfig, string $authFromConfig): bool;

  public function getRawRequest(): string;
}
