<?php

declare(strict_types=1);

namespace DivideBuySdk\Request\Incoming;

use DivideBuySdk\Constant\IncomingRequestParameters;

class ActivatePosSystemRequest extends AbstractRequest
{
  public function getActivatePos(): ?int
  {
    return $this->getIntegerValue(IncomingRequestParameters::ACTIVATE_POS);
  }

  public function getStoreCode(): string
  {
    return (string) $this->getParameterValue(IncomingRequestParameters::STORE_CODE);
  }
}
