<?php

declare(strict_types=1);

namespace DivideBuySdk\Request\Incoming;

use DivideBuySdk\Constant\IncomingRequestParameters;

class UpdateCourierRequest extends AbstractRequest
{
  public function getCouriers(): array
  {
    return (array) $this->getParameterValue(IncomingRequestParameters::IN_COURIERS);
  }
}
