<?php

declare(strict_types=1);

namespace DivideBuySdk\Request\Incoming;

use DivideBuySdk\Constant\IncomingRequestParameters;

class DeleteOrderRequest extends AbstractRequest
{
  public function getStoreOrderId(): int
  {
    return (int) $this->getParameterValue(IncomingRequestParameters::IN_STORE_ORDER_ID);
  }

  public function getDeleteUserOrder(): bool
  {
    return (bool) $this->getParameterValue(IncomingRequestParameters::IN_STORE_ORDER_ID);
  }
}
