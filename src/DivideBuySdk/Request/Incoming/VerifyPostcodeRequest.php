<?php

declare(strict_types=1);

namespace DivideBuySdk\Request\Incoming;

use DivideBuySdk\Constant\IncomingRequestParameters;

class VerifyPostcodeRequest extends AbstractRequest
{
  public function getOrderId(): int
  {
    return (int) $this->getParameterValue(IncomingRequestParameters::IN_ORDER_ID);
  }

  public function getUserPostcode()
  {
    return $this->getParameterValue(IncomingRequestParameters::IN_USER_POST_CODE);
  }
}
