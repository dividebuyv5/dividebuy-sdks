<?php

declare(strict_types=1);

namespace DivideBuySdk\Request\Incoming;

use DivideBuySdk\Constant\IncomingRequestParameters;
use DivideBuySdk\Data\OrderAddress;

class CreateCustomPosOrderRequest extends AbstractRequest
{
  public function getFirstName(): string
  {
    return (string) $this->getParameterValue(IncomingRequestParameters::IN_FIRST_NAME);
  }

  public function getLastName(): string
  {
    return (string) $this->getParameterValue(IncomingRequestParameters::IN_LAST_NAME);
  }

  public function getCustomerEmail(): string
  {
    return (string) $this->getParameterValue(IncomingRequestParameters::IN_CUSTOMER_EMAIL);
  }

  public function getOrderTotal(): float
  {
    return (float) $this->getParameterValue(IncomingRequestParameters::IN_ORDER_TOTAL);
  }

  public function getOrderTime(): string
  {
    return (string) $this->getParameterValue(IncomingRequestParameters::IN_ORDER_TIME);
  }

  public function getAddress(): OrderAddress
  {
    $data = (array) $this->getParameterValue(IncomingRequestParameters::IN_ADDRESS);

    return new OrderAddress($data);
  }
}
