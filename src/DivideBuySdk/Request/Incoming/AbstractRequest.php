<?php

declare(strict_types=1);

namespace DivideBuySdk\Request\Incoming;

use DivideBuySdk\Constant\IncomingRequestParameters;
use DivideBuySdk\Data\RequestPayload;
use DivideBuySdk\Exception\CredentialsMismatchException;
use DivideBuySdk\Exception\InvalidCredentialsFromConfigException;
use DivideBuySdk\Exception\InvalidCredentialsInRequestException;
use DivideBuySdk\Helper\ArrayUtilityTrait;

abstract class AbstractRequest implements IncomingRequestInterface
{
  use ArrayUtilityTrait;

  protected RequestPayload $content;

  protected string $storeToken;

  protected string $storeAuthentication;

  protected string $method;

  protected string $rawRequest;

  public function __construct(string $payload)
  {
    $this->rawRequest = $payload;
    $payload = json_decode($payload, true);
    $payload = $this->normaliseKeys($payload);
    $this->content = new RequestPayload($payload);

    $this->method = (string) $this->get($this->content, IncomingRequestParameters::IN_METHOD);
    $this->storeToken = $this->getStoreToken();
    $this->storeAuthentication = $this->getStoreAuth();
  }

  /**
   * @param string $tokenFromConfig
   * @param string $authFromConfig
   *
   * @return bool
   *
   * @throws InvalidCredentialsInRequestException
   * @throws CredentialsMismatchException
   * @throws InvalidCredentialsFromConfigException
   */
  public function validateCredentials(string $tokenFromConfig, string $authFromConfig): bool
  {
    if (empty($tokenFromConfig) || empty($authFromConfig)) {
      throw new InvalidCredentialsFromConfigException();
    }

    if (empty($this->storeToken) || empty($this->storeAuthentication)) {
      throw new InvalidCredentialsInRequestException();
    }

    if ($tokenFromConfig !== $this->storeToken ||
            $authFromConfig !== $this->storeAuthentication) {
      throw new CredentialsMismatchException();
    }

    return true;
  }

  public function getOrderId(): int
  {
    return (int) $this->getParameterValue(IncomingRequestParameters::IN_ORDER_ID);
  }

  public function getRawRequest(): string
  {
    return (string) $this->rawRequest;
  }

  public function getMethod(): string
  {
    return (string) $this->method;
  }

  public function getStoreAuth(): string
  {
    return (string) $this->getParameterValue(IncomingRequestParameters::IN_STORE_AUTH, '');
  }

  public function getStoreToken(): string
  {
    return (string) $this->getParameterValue(IncomingRequestParameters::IN_STORE_TOKEN, '');
  }

  public function getRetailerStoreCode(): string
  {
    return (string) $this->getParameterValue(IncomingRequestParameters::IN_RETAILER_STORE_CODE);
  }

  public function getContent(): RequestPayload
  {
    return $this->content;
  }

  /**
   * @param string $key
   * @param mixed $default
   *
   * @return mixed|string|array
   */
  public function getParameterValue(string $key, $default = null)
  {
    $possibleKeys = [$key, self::camel($key), self::snake($key)];
    $possibleKeys = array_unique($possibleKeys);

    foreach ($possibleKeys as $key) {
      $value = $this->get($this->content, $key, $default);
      if ($value !== null) {
        return $value;
      }
    }

    return $default;
  }

  public function getIntegerValue($key)
  {
    $value = $this->getParameterValue($key);

    if (is_numeric($value)) {
      return (int) $value;
    }

    return $value;
  }
}
