<?php

declare(strict_types=1);

namespace DivideBuySdk\Request\Incoming;

class CancelOrderRequest extends DeleteOrderRequest
{
}
