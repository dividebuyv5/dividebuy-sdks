<?php

declare(strict_types=1);

namespace DivideBuySdk\Request;

use DivideBuySdk\Constant\Endpoints;
use DivideBuySdk\Constant\IncomingRequestParameters;
use DivideBuySdk\Constant\Methods;
use DivideBuySdk\Exception\InvalidRequestPayloadException;
use DivideBuySdk\Helper\ArrayUtilityTrait;
use DivideBuySdk\Request\Incoming\ActivatePosSystemRequest;
use DivideBuySdk\Request\Incoming\CancelOrderRequest;
use DivideBuySdk\Request\Incoming\CreateCustomPosOrderRequest;
use DivideBuySdk\Request\Incoming\DeleteOrderRequest;
use DivideBuySdk\Request\Incoming\GetOrderDetailsRequest;
use DivideBuySdk\Request\Incoming\IncomingRequestInterface;
use DivideBuySdk\Request\Incoming\OrderSuccessRequest;
use DivideBuySdk\Request\Incoming\RetailerConfigurationRequest;
use DivideBuySdk\Request\Incoming\UpdateCourierRequest;
use DivideBuySdk\Request\Incoming\VerifyPostcodeRequest;
use DivideBuySdk\Request\Outgoing\CheckAccessRequest;
use DivideBuySdk\Request\Outgoing\ConfirmRetailerRequest;
use DivideBuySdk\Request\Outgoing\GetUserOrderRequest;
use DivideBuySdk\Request\Outgoing\OutgoingRequestInterface;
use DivideBuySdk\Request\Outgoing\RefundRequest;
use DivideBuySdk\Request\Outgoing\SoftSearchKeyRequest;
use DivideBuySdk\Request\Outgoing\SyncRetailerOrderRequest;
use DivideBuySdk\Request\Outgoing\TrackingRequest;
use DivideBuySdk\Request\Outgoing\UpdateRetailerStatusRequest;
use DivideBuySdk\Request\Outgoing\VerifyTokenPermissionRequest;
use InvalidArgumentException;

class RequestFactory
{
  use ArrayUtilityTrait;

  public function createFromEndpoint($filePath, $endPoint): IncomingRequestInterface
  {
    $payload = trim((string) file_get_contents($filePath));
    $content = json_decode($payload, true);
    $this->validateContent($content);

    switch ($endPoint) {
      case Endpoints::URL_ACTIVATE_POS:
        return new ActivatePosSystemRequest($payload);
      case Endpoints::URL_CREATE_CUSTOM_POS_ORDER:
        return new CreateCustomPosOrderRequest($payload);
      default:
        throw new InvalidArgumentException("Request endpoint [{$endPoint}] is not implemented");
    }
  }

  public function createIncoming(string $filePath, string $defaultMethod = ''): IncomingRequestInterface
  {
    $payload = trim((string) file_get_contents($filePath));
    $content = json_decode($payload, true);
    $this->validateContent($content);
    $method = $this->get($content, IncomingRequestParameters::IN_METHOD, $defaultMethod);

    switch ($method) {
      case Methods::UPDATE_COURIERS:
        return new UpdateCourierRequest($payload);
      case Methods::VERIFY_POSTCODE:
        return new VerifyPostcodeRequest($payload);
      case Methods::CANCEL_ORDER:
        return new CancelOrderRequest($payload);
      case Methods::DELETE_ORDER:
        return new DeleteOrderRequest($payload);
      case Methods::ORDER_SUCCESS:
        return new OrderSuccessRequest($payload);
      case Methods::RETAILER_CONFIGURATIONS:
        return new RetailerConfigurationRequest($payload);
      case Methods::GET_ORDER_DETAILS:
        return new GetOrderDetailsRequest($payload);
      default:
        throw new InvalidArgumentException("Request method {$method} is not implemented");
    }
  }

  public function createOutgoing(
      string $endpoint,
      array $queryParams = [],
      array $postParams = []
  ): OutgoingRequestInterface {
    switch ($endpoint) {
        case Endpoints::URL_SYNC_RETAILER_ORDER:
  return new SyncRetailerOrderRequest($queryParams, $postParams);
        case Endpoints::URL_USER_ORDER:
  return new GetUserOrderRequest($queryParams, $postParams);
        case Endpoints::URL_UPDATE_RETAILER_STATUS:
  return new UpdateRetailerStatusRequest($queryParams, $postParams);
        case Endpoints::URL_REFUND:
  return new RefundRequest($queryParams, $postParams);
        case Endpoints::URL_CHECK_ACCESS:
  return new CheckAccessRequest($queryParams, $postParams);
        case Endpoints::URL_CONFIRM_RETAILER:
  return new ConfirmRetailerRequest($queryParams, $postParams);
        case Endpoints::URL_TRACKING:
  return new TrackingRequest($queryParams, $postParams);
        case Endpoints::URL_GET_SOFT_SEARCH_KEY:
  return new SoftSearchKeyRequest($queryParams, $postParams);
        case Endpoints::URL_VERIFY_TOKEN_PERMISSION:
  return new VerifyTokenPermissionRequest($queryParams, $postParams);
        default:
  throw new InvalidArgumentException("Request endpoint {$endpoint} is not implemented");
    }
  }

  /**
   * @param mixed $content
   */
  private function validateContent($content)
  {
    if (empty($content)) {
      throw new InvalidRequestPayloadException('Request payload is invalid', 400);
    }
  }
}
