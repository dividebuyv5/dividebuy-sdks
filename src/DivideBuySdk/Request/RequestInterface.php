<?php

declare(strict_types=1);

namespace DivideBuySdk\Request;

interface RequestInterface
{
  public function getMethod(): string;
}
