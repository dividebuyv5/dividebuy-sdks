<?php

declare(strict_types=1);

namespace DivideBuySdk\Request\Outgoing;

use DivideBuySdk\Constant\Endpoints;

class VerifyTokenPermissionRequest implements OutgoingRequestInterface
{
  use RequestTrait;

  protected string $method = 'POST';

  protected array $fields = [
      'token' => [],
  ];

  public function getEndpoint(): string
  {
    return Endpoints::URL_VERIFY_TOKEN_PERMISSION;
  }

  public function validateParams(): bool
  {
    $this->validate($this->postParams, $this->fields);

    return true;
  }
}
