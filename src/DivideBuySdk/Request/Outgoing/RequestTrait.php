<?php

declare(strict_types=1);

namespace DivideBuySdk\Request\Outgoing;

use DivideBuySdk\Exception\InvalidParameterException;
use DivideBuySdk\Exception\MissingParameterException;

trait RequestTrait
{
  protected string $domain;

  protected array $queryParams = [];

  protected array $postParams = [];

  public function __construct(array $queryParams = [], array $postParams = [])
  {
    $this->queryParams = $queryParams;
    $this->postParams = $postParams;

    $this->validateParams();
  }

  public function getQueryParams(): array
  {
    return $this->queryParams;
  }

  public function getPostParams(): array
  {
    return $this->postParams;
  }

  public function getMethod(): string
  {
    return $this->method;
  }

  protected function validate(array $data, array $requiredFields, ?string $section = null): bool
  {
    $data = empty($section) ? $data : $data[$section] ?? [];
    $fieldNames = array_keys($requiredFields);

    foreach ($fieldNames as $field) {
      if (! array_key_exists($field, $data)) {
        $message = trim("{$section}: {$field} is required", ': ');
        throw new MissingParameterException($message, 400);
      }

      $value = $data[$field];

      if (empty($value) && $value !== false && $value !== '0') {
        $error = "{$field} cannot be empty";
        throw  new InvalidParameterException($error, 400);
      }

      if (! empty($requiredFields[$field])) {
        if (! in_array($value, $requiredFields[$field])) {
          $allowed = implode(', ', $requiredFields[$field]);
          $error = "{$field} must be one of the following: {$allowed}";
          throw  new InvalidParameterException($error, 400);
        }
      }
    }

    return true;
  }
}
