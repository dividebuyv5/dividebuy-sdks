<?php

declare(strict_types=1);

namespace DivideBuySdk\Request\Outgoing;

use DivideBuySdk\Request\RequestInterface;

interface OutgoingRequestInterface extends RequestInterface
{
  public function getQueryParams(): array;

  public function getPostParams(): array;

  public function getEndpoint(): string;

  public function validateParams(): bool;
}
