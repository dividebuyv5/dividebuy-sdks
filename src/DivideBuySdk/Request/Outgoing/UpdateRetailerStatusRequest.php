<?php

declare(strict_types=1);

namespace DivideBuySdk\Request\Outgoing;

use DivideBuySdk\Constant\Endpoints;

class UpdateRetailerStatusRequest implements OutgoingRequestInterface
{
  use RequestTrait;

  protected string $method = 'POST';

  protected array $fields = [
      'retailerStatus' => ['1', '0'],
  ];

  public function getEndpoint(): string
  {
    return Endpoints::URL_UPDATE_RETAILER_STATUS;
  }

  public function validateParams(): bool
  {
    $this->validate($this->postParams, $this->fields);

    return true;
  }
}
