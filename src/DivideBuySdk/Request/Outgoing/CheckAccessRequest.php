<?php

declare(strict_types=1);

namespace DivideBuySdk\Request\Outgoing;

use DivideBuySdk\Constant\Endpoints;

class CheckAccessRequest implements OutgoingRequestInterface
{
  use RequestTrait;

  protected string $method = 'GET';

  public function getEndpoint(): string
  {
    return Endpoints::URL_CHECK_ACCESS;
  }

  public function validateParams(): bool
  {
    return true;
  }
}
