<?php

declare(strict_types=1);

namespace DivideBuySdk\Request\Outgoing;

use DivideBuySdk\Constant\Endpoints;

class SyncRetailerOrderRequest implements OutgoingRequestInterface
{
  use RequestTrait;

  protected string $method = 'POST';

  protected array $fields = [
      'storeOrderId' => [],
  ];

  public function getEndpoint(): string
  {
    return Endpoints::URL_SYNC_RETAILER_ORDER;
  }

  public function validateParams(): bool
  {
    $this->validate($this->postParams, $this->fields);

    return true;
  }
}
