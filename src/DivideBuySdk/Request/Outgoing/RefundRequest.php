<?php

declare(strict_types=1);

namespace DivideBuySdk\Request\Outgoing;

use DivideBuySdk\Constant\Endpoints;

class RefundRequest implements OutgoingRequestInterface
{
  use RequestTrait;

  protected string $method = 'POST';

  private array $rootFields = [
      'product' => [],
      'totalRefund' => [],
      'orderId' => [],
      'refundType' => [],
      'retailer' => [],
  ];

  private array $productFields = [
      'sku' => [],
      'qty' => [],
      'productName' => [],
      'rowInclTotal' => [],
  ];

  private array $retailerFields = [
      'storeAuthentication' => [],
      'storeToken' => [],
  ];

  public function getEndpoint(): string
  {
    return Endpoints::URL_REFUND;
  }

  public function validateParams(): bool
  {
    $this->validate($this->postParams, $this->rootFields);
    $this->validate($this->postParams, $this->retailerFields, 'retailer');

    foreach ($this->postParams['product'] as $products) {
      $this->validate($products, $this->productFields);
    }

    return true;
  }
}
