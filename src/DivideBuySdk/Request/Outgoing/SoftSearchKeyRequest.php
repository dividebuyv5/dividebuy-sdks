<?php

declare(strict_types=1);

namespace DivideBuySdk\Request\Outgoing;

use DivideBuySdk\Constant\Endpoints;

class SoftSearchKeyRequest implements OutgoingRequestInterface
{
  use RequestTrait;

  protected string $method = 'POST';

  protected array $fields = [
      'retailer_base_url' => [],
      'order_total' => [],
  ];

  public function getEndpoint(): string
  {
    return Endpoints::URL_GET_SOFT_SEARCH_KEY;
  }

  public function validateParams(): bool
  {
    $this->validate($this->postParams, $this->fields);

    return true;
  }
}
