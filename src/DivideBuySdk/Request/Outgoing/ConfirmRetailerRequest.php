<?php

declare(strict_types=1);

namespace DivideBuySdk\Request\Outgoing;

use DivideBuySdk\Constant\Endpoints;

class ConfirmRetailerRequest implements OutgoingRequestInterface
{
  use RequestTrait;

  protected string $method = 'PUT';

  public function getEndpoint(): string
  {
    return Endpoints::URL_CONFIRM_RETAILER;
  }

  public function validateParams(): bool
  {
    return true;
  }
}
