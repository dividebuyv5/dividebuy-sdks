<?php

declare(strict_types=1);

namespace DivideBuySdk\Request\Outgoing;

use DivideBuySdk\Constant\Endpoints;

class TrackingRequest implements OutgoingRequestInterface
{
  use RequestTrait;

  protected string $method = 'POST';

  protected array $fields = [
      'storeOrderId' => [],
      'trackingInfo' => [],
      'productDetails' => [],
  ];

  protected array $trackingFields = [
      'trackNumber' => [],
      'title' => [],
  ];

  public function getEndpoint(): string
  {
    return Endpoints::URL_TRACKING;
  }

  public function validateParams(): bool
  {
    $this->validate($this->postParams, $this->fields);
    foreach ($this->postParams['trackingInfo'] as $data) {
      $this->validate($data, $this->trackingFields);
    }

    return true;
  }
}
