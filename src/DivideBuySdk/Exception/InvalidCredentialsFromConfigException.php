<?php

declare(strict_types=1);

namespace DivideBuySdk\Exception;

use InvalidArgumentException;

class InvalidCredentialsFromConfigException extends InvalidArgumentException
{
  /**
   * @var string
   */
  protected $message = 'Invalid Store Credentials Provided';

  /**
   * @var int
   */
  protected $code = 401;
}
