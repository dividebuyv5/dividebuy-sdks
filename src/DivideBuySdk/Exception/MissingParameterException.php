<?php

declare(strict_types=1);

namespace DivideBuySdk\Exception;

use InvalidArgumentException;

class MissingParameterException extends InvalidArgumentException
{
}
