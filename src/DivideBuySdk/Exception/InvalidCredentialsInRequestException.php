<?php

declare(strict_types=1);

namespace DivideBuySdk\Exception;

use InvalidArgumentException;

class InvalidCredentialsInRequestException extends InvalidArgumentException
{
  /**
   * @var string
   */
  protected $message = 'Invalid Store Credentials Provided In Request';

  /**
   * @var int
   */
  protected $code = 400;
}
