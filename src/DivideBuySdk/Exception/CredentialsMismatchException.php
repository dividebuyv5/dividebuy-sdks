<?php

declare(strict_types=1);

namespace DivideBuySdk\Exception;

use InvalidArgumentException;

class CredentialsMismatchException extends InvalidArgumentException
{
  /**
   * @var string
   */
  protected $message = 'Store Credentials Mismatch';

  /**
   * @var int
   */
  protected $code = 401;
}
