<?php

declare(strict_types=1);

namespace DivideBuySdk\Api;

use DivideBuySdk\Api;
use DivideBuySdk\Client\HttpClient;
use DivideBuySdk\Request\RequestFactory;
use DivideBuySdk\Response\ResponseFactory;
use DivideBuySdk\Service\Authentication\Parameters;
use DivideBuySdk\Service\DomainResolver;
use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class ApiFactory
{
  public static function create(
      ?string $storeToken,
      ?string $storeAuth,
      ?string $env = '',
      ?LoggerInterface $logger = null
  ): Api {
    $appLogger = $logger ? $logger : new NullLogger();
    $parameters = new Parameters((string) $storeToken, (string) $storeAuth);
    $domainResolver = new DomainResolver($appLogger, $env);
    $reqFactory = new RequestFactory();
    $resFactory = new ResponseFactory();

    $client = new Client(
        [
            'http_errors' => false,
            'headers' => ['content-type' => 'application/json'],
        ]
    );

    $httpClient = new HttpClient($client, $parameters, $domainResolver);
    $incoming = new IncomingApi($parameters, $reqFactory, $resFactory, $appLogger);
    $outgoing = new OutgoingApi($httpClient, $parameters, $reqFactory, $resFactory, $appLogger);

    return new Api($incoming, $outgoing, $domainResolver, $appLogger);
  }
}
