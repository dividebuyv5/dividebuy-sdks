<?php

declare(strict_types=1);

namespace DivideBuySdk\Api;

use DivideBuySdk\Constant\Endpoints;
use DivideBuySdk\Constant\Methods;
use DivideBuySdk\Data\ResponsePayload;
use DivideBuySdk\Request\RequestFactory;
use DivideBuySdk\Response\Outgoing\GetOrderDetailsResponse;
use DivideBuySdk\Response\ResponseFactory;
use DivideBuySdk\Service\Authentication\Parameters;
use Psr\Log\LoggerInterface;
use Throwable;

final class IncomingApi extends AbstractApi
{
  public const DEFAULT_REQUEST_SOURCE = 'php://input';

  protected string $requestSource = self::DEFAULT_REQUEST_SOURCE;

  public function __construct(
      Parameters $parameters,
      RequestFactory $requestFactory,
      ResponseFactory $responseFactory,
      LoggerInterface $logger
  ) {
    $this->parameters = $parameters;
    $this->requestFactory = $requestFactory;
    $this->responseFactory = $responseFactory;
    $this->setLogger($logger);
  }

  public function getRequestMethod(): string
  {
    $request = $this->requestFactory->createIncoming($this->getRequestSource());

    return (string) $request->getMethod();
  }

  public function processFetchCouriers(callable $callbackFunction): array
  {
    try {
      $response = [
          'status' => 'ok',
          'couriers' => $callbackFunction(),
      ];
      $this->logger->debug(__METHOD__, $response);

      return $response;
    } catch (Throwable $exception) {
      $this->logger->error(__METHOD__ . ' - Exception occurred', [
          'exception' => (string) $exception,
      ]);

      return $this->responseFactory->createCommonResponse()->getFailurePayload($exception);
    }
  }

  public function processCreateCustomPosOrder(callable $callbackFunction): array
  {
    $this->request = $this->requestFactory->createFromEndpoint(
        $this->getRequestSource(),
        Endpoints::URL_CREATE_CUSTOM_POS_ORDER
    );

    $response = $this->process(Endpoints::URL_ACTIVATE_POS, $callbackFunction);

    return $this->responseFactory->getOutgoingResponse(
        Endpoints::URL_CREATE_CUSTOM_POS_ORDER,
        new ResponsePayload($response)
    )
        ->getSuccessPayload();
  }

  public function processActivatePos(callable $callbackFunction): array
  {
    $this->request = $this->requestFactory->createFromEndpoint(
        $this->getRequestSource(),
        Endpoints::URL_ACTIVATE_POS
    );

    $this->process(Endpoints::URL_ACTIVATE_POS, $callbackFunction);

    return $this->responseFactory->createCommonResponse()->getSuccessPayload();
  }

  public function processVerifyPostcode(callable $callbackFunction): array
  {
    try {
      $this->processCustomApiMethod(Methods::VERIFY_POSTCODE, $callbackFunction);
      $response = $this->responseFactory->create($this->request, Methods::VERIFY_POSTCODE);

      return $response->getSuccessPayload();
    } catch (Throwable $exception) {
      $this->logger->error(__METHOD__ . ' - Exception occurred', [
          'exception' => (string) $exception,
          'requestClass' => get_class($this->request),
      ]);

      return $this->responseFactory->createCommonResponse()->getFailurePayload($exception);
    }
  }

  public function processGetOrderDetails(callable $callbackFunction): array
  {
    try {
      $orderDetails = $this->processCustomApiMethod(Methods::GET_ORDER_DETAILS, $callbackFunction);
      /** @var GetOrderDetailsResponse $response */
      $response = $this->responseFactory->create($this->request, Methods::GET_ORDER_DETAILS);

      return $response->setOrderDetails($orderDetails)->toArray();
    } catch (Throwable $exception) {
      $this->logger->error(__METHOD__ . ' - Exception occurred', [
          'exception' => (string) $exception,
          'requestClass' => get_class($this->request),
      ]);

      return $this->responseFactory->createCommonResponse()->getFailurePayload($exception);
    }
  }

  /**
   * Extensions calling the method should pass in a callback function which implements
   * the framework specific code for the method in the request. A request object would
   * be passed as the first argument to the callback function.
   *
   * The callback function should throw an exception in the event of a failure. The exception
   * should have a message and a code. This will be returned in the response as:
   *
   * status = exception->getCode()
   * message = exception->getMessage()
   *
   * @param  callable  $callbackFunction
   *
   * @return array
   */
  public function processIncomingRequest(callable $callbackFunction): array
  {
    $this->request = $this->requestFactory->createIncoming($this->getRequestSource());

    try {
      $this->request->validateCredentials(
          $this->parameters->getStoreToken(),
          $this->parameters->getStoreAuthentication()
      );

      $this->logger->debug(__METHOD__ . ' - Processing request ... ', [
          'requestClass' => get_class($this->request),
      ]);

      $callbackFunction($this->request);

      $response = $this->responseFactory->create($this->request);
      $payload = $response->getSuccessPayload();

      $this->logger->debug(__METHOD__ . ' - Request processed ', [
          'requestClass' => get_class($this->request),
          'responseClass' => get_class($response),
          'payload' => $payload,
      ]);

      return $payload;
    } catch (Throwable $exception) {
      $this->logger->error(__METHOD__ . ' - Exception occurred', [
          'exception' => (string) $exception,
          'requestClass' => get_class($this->request),
      ]);

      return $this->responseFactory->createCommonResponse()->getFailurePayload($exception);
    }
  }

  public function getRequestSource(): string
  {
    return $this->requestSource;
  }

  public function setRequestSource(string $requestSource): void
  {
    $this->requestSource = $requestSource;
  }

  /**
   * @param  string  $method
   * @param  callable  $callbackFunction
   *
   * @return mixed
   */
  private function processCustomApiMethod(string $method, callable $callbackFunction)
  {
    $this->request = $this->requestFactory->createIncoming(
        $this->getRequestSource(),
        $method
    );

    return $this->process($method, $callbackFunction);
  }

  private function process($method, $callbackFunction)
  {
    $this->request->validateCredentials(
        $this->parameters->getStoreToken(),
        $this->parameters->getStoreAuthentication()
    );

    $this->logger->debug($method . ' - Processing request ... ', [
        'requestClass' => get_class($this->request),
    ]);

    $return = $callbackFunction($this->request);

    $this->logger->debug(__METHOD__ . ' - Request processed ', [
        'requestClass' => get_class($this->request),
        'returnData' => $return,
    ]);

    return $return;
  }
}
