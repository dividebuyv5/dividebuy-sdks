<?php

declare(strict_types=1);

namespace DivideBuySdk\Api;

use DivideBuySdk\Client\HttpClient;
use DivideBuySdk\Request\RequestFactory;
use DivideBuySdk\Response\ResponseFactory;
use DivideBuySdk\Service\Authentication\Parameters;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;

final class OutgoingApi extends AbstractApi
{
  public function __construct(
      HttpClient $httpClient,
      Parameters $parameters,
      RequestFactory $requestFactory,
      ResponseFactory $responseFactory,
      LoggerInterface $logger
  ) {
    $this->httpClient = $httpClient;
    $this->parameters = $parameters;
    $this->requestFactory = $requestFactory;
    $this->responseFactory = $responseFactory;
    $this->setLogger($logger);
  }

  /**
   * @param string $apiName
   * @param array $queryParams
   * @param array $postParams
   *
   * @return array
   *
   * @throws GuzzleException
   */
  public function sendRequest(string $apiName, array $queryParams = [], array $postParams = []): array
  {
    $this->logger->debug(__METHOD__ . ' - Sending API request ...', [
        'apiName' => $apiName,
        'queryParams' => $queryParams,
        'postParams' => $postParams,
        'apiDomain' => $this->httpClient->getUrlResolver()->getApiDomain(),
    ]);

    $this->request = $this->requestFactory->createOutgoing($apiName, $queryParams, $postParams);
    $response = $this->responseFactory->create($this->request);

    $return = $this->httpClient->send($this->request, $response);

    $this->logger->debug(__METHOD__ . ' - Request completed', [
        'apiName' => $apiName,
        'responseBody' => (string) $response->getResult()->getBody(),
        'apiDomain' => $this->httpClient->getUrlResolver()->getApiDomain(),
        'returnData' => $return,
    ]);

    return $return;
  }
}
