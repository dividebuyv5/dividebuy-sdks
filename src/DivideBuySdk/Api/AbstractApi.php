<?php

declare(strict_types=1);

namespace DivideBuySdk\Api;

use DivideBuySdk\Client\HttpClient;
use DivideBuySdk\Request\Incoming\IncomingRequestInterface;
use DivideBuySdk\Request\Outgoing\OutgoingRequestInterface;
use DivideBuySdk\Request\RequestFactory;
use DivideBuySdk\Request\RequestInterface;
use DivideBuySdk\Response\ResponseFactory;
use DivideBuySdk\Service\Authentication\Parameters;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

class AbstractApi implements LoggerAwareInterface
{
  use LoggerAwareTrait;

  protected HttpClient $httpClient;

  protected Parameters $parameters;

  protected RequestFactory $requestFactory;

  protected ResponseFactory $responseFactory;

  protected RequestInterface $request;

  /**
   * @return IncomingRequestInterface|OutgoingRequestInterface
   */
  public function getRequest(): RequestInterface
  {
    return $this->request;
  }
}
