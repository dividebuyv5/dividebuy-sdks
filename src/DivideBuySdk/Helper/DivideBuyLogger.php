<?php

declare(strict_types=1);

namespace DivideBuySdk\Helper;

use Psr\Log\AbstractLogger;

class DivideBuyLogger extends AbstractLogger
{
  public function log($level, $message, array $context = [])
  {
    $contextString = $context ? json_encode($context) : '';
    error_log(trim("{$level}: {$message} : {$contextString}", ' :'));
  }
}
