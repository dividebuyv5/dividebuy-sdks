<?php

declare(strict_types=1);

namespace DivideBuySdk\Helper;

use Closure;
use Dotenv\Repository\Adapter\PutenvAdapter;
use Dotenv\Repository\RepositoryBuilder;
use Dotenv\Repository\RepositoryInterface;
use PhpOption\Option;

trait EnvTrait
{
  /**
   * Indicates if the putenv adapter is enabled.
   *
   * @var bool
   */
  protected static bool $putenv = true;

  /**
   * The environment repository instance.
   *
   * @var mixed
   */
  protected static $repository = null;

  /**
   * Get the environment repository instance.
   *
   * @return RepositoryInterface
   */
  public static function getRepository(): RepositoryInterface
  {
    if (static::$repository === null) {
      $builder = RepositoryBuilder::createWithDefaultAdapters();

      if (static::$putenv) {
        $builder = $builder->addAdapter(PutenvAdapter::class);
      }

      static::$repository = $builder->immutable()->make();
    }

    return static::$repository;
  }

  /**
   * Gets the value of an environment variable.
   *
   * @param string $key
   * @param mixed $default
   *
   * @return mixed
   */
  public static function getEnv(string $key, $default = null)
  {
    return Option::fromValue(static::getRepository()->get($key))
        ->map(static function ($value) {
          switch (strtolower($value)) {
                    case 'true':
                    case '(true)':
                        return true;
                    case 'false':
                    case '(false)':
                        return false;
                    case 'empty':
                    case '(empty)':
                        return '';
                    case 'null':
                    case '(null)':
                        return null;
          }

          if (preg_match('/\A([\'"])(.*)\1\z/', $value, $matches)) {
            return $matches[2];
          }

          return $value;
        })
        ->getOrCall(static function () use ($default) {
          return $default instanceof Closure ? $default() : $default;
        });
  }
}
