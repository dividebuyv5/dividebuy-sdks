<?php

declare(strict_types=1);

namespace DivideBuySdk\Helper;

use ArrayAccess;

trait ArrayUtilityTrait
{
  use EnvTrait;

  protected static array $snakeCache = [];

  protected static array $studlyCache = [];

  protected static array $camelCache = [];

  /**
   * @param mixed $value
   *
   * @return bool
   */
  public static function accessible($value): bool
  {
    return is_array($value) || $value instanceof ArrayAccess;
  }

  /**
   * @param ArrayAccess|array $array
   * @param string|int $key
   *
   * @return bool
   */
  public static function exists($array, $key): bool
  {
    if ($array instanceof ArrayAccess) {
      return $array->offsetExists($key);
    }

    return array_key_exists($key, $array);
  }

  public static function value($value)
  {
    return is_callable($value) ? $value() : $value;
  }

  /**
   * Get an item from an array using "dot" notation.
   *
   * @param ArrayAccess|array $array
   * @param string|int|null $key
   * @param mixed $default
   *
   * @return mixed
   */
  public static function get($array, $key, $default = null)
  {
    if (! static::accessible($array)) {
      return self::value($default);
    }

    if ($key === null) {
      return $array;
    }

    if (static::exists($array, $key)) {
      return $array[$key];
    }

    if (strpos($key, '.') === false) {
      return $array[$key] ?? self::value($default);
    }

    foreach (explode('.', $key) as $segment) {
      if (! (static::accessible($array) && static::exists($array, $segment))) {
        return self::value($default);
      }
      $array = $array[$segment];
    }

    return $array;
  }

  public static function camel(string $value): string
  {
    if (isset(static::$camelCache[$value])) {
      return static::$camelCache[$value];
    }

    return static::$camelCache[$value] = lcfirst(static::studly($value));
  }

  public static function studly(string $value): string
  {
    $key = $value;

    if (isset(static::$studlyCache[$key])) {
      return static::$studlyCache[$key];
    }

    $value = ucwords(str_replace(['-', '_'], ' ', $value));

    return static::$studlyCache[$key] = str_replace(' ', '', $value);
  }

  public static function snake(string $value, string $delimiter = '_'): string
  {
    $key = $value;

    if (isset(static::$snakeCache[$key][$delimiter])) {
      return static::$snakeCache[$key][$delimiter];
    }

    if (! ctype_lower($value)) {
      $value = preg_replace('/\s+/u', '', ucwords($value));

      $value = static::lower(preg_replace('/(.)(?=[A-Z])/u', '$1' . $delimiter, $value));
    }

    return static::$snakeCache[$key][$delimiter] = $value;
  }

  public static function lower(string $value): string
  {
    return mb_strtolower($value, 'UTF-8');
  }

  public function normaliseKeys(array $data): array
  {
    $list = $data;
    foreach ($data as $key => $value) {
      $newKey = $this->snake((string) $key);
      $list[$newKey] = is_array($value) ? $this->normaliseKeys($value) : $value;
    }
    ksort($list);

    return $list;
  }
}
