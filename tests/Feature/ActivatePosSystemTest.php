<?php

namespace Tests\Feature;

use DivideBuySdk\Request\Incoming\ActivatePosSystemRequest;
use Tests\BaseApiTestCase;

class ActivatePosSystemTest extends BaseApiTestCase
{
  private string $sampleRequest;

  public function setUp(): void
  {
    parent::setUp();
    $this->sampleRequest = __DIR__ . '/../Fixtures/activatePos.json';
  }

  public function testActivatePosRequestCanBeProcessed()
  {
    $callback = function ($request) {
      $this->validateRequestParameters($request);

      return true;
    };
    $this->api->setRequestSource($this->sampleRequest);
    $response = $this->api->activatePosSystem($callback);

    $this->validateSuccessResponse($response);
  }

  private function validateRequestParameters(ActivatePosSystemRequest $request)
  {
    $pos = $request->getIntegerValue('activate_pos_none');
    $this->assertEmpty($pos);

    $this->assertSame('store-code', $request->getStoreCode());
    $this->assertSame(1, $request->getActivatePos());
    $this->assertSame('retailer-store-code', $request->getRetailerStoreCode());
  }
}
