<?php

namespace Tests\Feature;

use DivideBuySdk\Api\ApiFactory;
use DivideBuySdk\Service\DomainResolver;
use Tests\BaseApiTestCase;

class TrackOrderTest extends BaseApiTestCase
{
  public function testThatTrackingApiCanBeCalledForATrackedOrder()
  {
    $payload = [
            'retailerId' => '150',
            'storeOrderId' => '6667',
            'storeToken' => '5LIH1TaW8ewd',
            'storeAuthentication' => '3aa7Sgt76sz7',
            'deleteTracking' => 0,
            'trackingInfo' =>
                [
                    [
                        'trackNumber' => '123456789',
                        'description' => null,
                        'title' => 'DPD Local',
                        'carrierCode' => 'DPD Local',
                    ],
                ],
            'productDetails' =>
                [
                    0 =>
                        [
                            'sku' => 'abl003',
                            'qty' => 1,
                        ],
                ],
        ];
    $this->api = (new ApiFactory())->create('5LIH1TaW8ewd', '3aa7Sgt76sz7', DomainResolver::ENV_LOCAL);

    $response = $this->api->trackOrder($payload);

    $this->assertIsArray($response);
    $this->assertArrayHasKey('message', $response);
    $this->assertArrayHasKey('error', $response);
    $this->assertSame('Could not process the following tracking numbers ( Invalid product sku abl003 )', $response['message']);
    $this->assertSame(1, $response['error']);
  }
}
