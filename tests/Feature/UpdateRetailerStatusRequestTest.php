<?php

namespace Tests\Feature;

use DivideBuySdk\Api\ApiFactory;
use DivideBuySdk\Exception\InvalidParameterException;
use Tests\BaseApiTestCase;

class UpdateRetailerStatusRequestTest extends BaseApiTestCase
{
  public function testThatUpdateRetailerStatusRequestReturnsSuccessfully()
  {
    $postParams = [
            'allowedIps' => '127.0.0.1',
            'retailerStatus' => '1',
        ];
    $this->api = (new ApiFactory())->create('5LIH1TaW8ewd', '3aa7Sgt76sz7', 'staging');
    $response = $this->api->updateRetailerStatus($postParams);

    $this->assertIsArray($response);
    $this->assertArrayHasKey('status', $response);
    $this->assertSame('ok', $response['status']);
  }

  public function testThatExceptionIsThrownWithInvalidRetailerStatus()
  {
    $this->expectException(InvalidParameterException::class);
    $this->expectExceptionMessage('retailerStatus must be one of the following: 1, 0');
    $postParams = ['retailerStatus' => '99',];
    $this->api->updateRetailerStatus($postParams);
  }
}
