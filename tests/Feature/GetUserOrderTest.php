<?php

namespace Tests\Feature;

use DivideBuySdk\Api\ApiFactory;
use DivideBuySdk\Helper\DivideBuyLogger;
use DivideBuySdk\Service\DomainResolver;
use GuzzleHttp\Exception\GuzzleException;
use Prophecy\Argument;
use Tests\BaseApiTestCase;

class GetUserOrderTest extends BaseApiTestCase
{
  /**
   * @throws GuzzleException
   */
  public function testThatGetUserOrderReturnsSuccessfully()
  {
    $this->api = (new ApiFactory())->create(
            '5LIH1TaW8ewd',
            '3aa7Sgt76sz7',
            DomainResolver::ENV_LOCAL,
            new DivideBuyLogger()
        );
    $response = $this->api->getUserOrder('6667');

    $this->assertIsArray($response);
    $this->assertArrayHasKey('trackingDetails', $response);
    $this->assertArrayHasKey('method', $response);
    $this->assertArrayHasKey('address', $response);
    $this->assertArrayHasKey('products', $response);
    $this->assertArrayHasKey('grandTotal', $response);
    $this->assertArrayHasKey('store_order_id', $response);
    $this->assertSame('6667', $response['store_order_id']);
  }

  /**
   * @throws GuzzleException
   */
  public function testThatGetUserOrderWithInvalidOrderReturnsError()
  {
    $this->api = (new ApiFactory())->create('5LIH1TaW8ewd', '3aa7Sgt76sz7', DomainResolver::ENV_LOCAL);
    $response = $this->api->getUserOrder('10666777777');

    $this->assertIsArray($response);
    $this->assertArrayHasKey('error', $response);
    $this->assertSame('Order not found for retailer', $response['error']['message']);
    $this->assertSame(422, $response['error']['status_code']);
  }
}
