<?php

namespace Tests\Feature;

use DivideBuySdk\Service\DomainResolver;
use Tests\BaseApiTestCase;

class CheckAccessTest extends BaseApiTestCase
{
  public function testThatCheckAccessReturnsSuccessfully()
  {
    $response = $this->api->checkAccess();
    $this->assertInstanceOf(DomainResolver::class, $this->api->getUrlResolver());

    $this->assertIsArray($response);
    $this->assertArrayHasKey('status', $response);
    $this->assertArrayHasKey('message', $response);
    $this->assertSame('ok', $response['status']);
    $this->assertSame('You are allowed to access', $response['message']);
  }
}
