<?php

namespace Tests\Feature;

use DivideBuySdk\Constant\Methods;
use DivideBuySdk\Request\Incoming\UpdateCourierRequest;
use Tests\BaseApiTestCase;

class UpdateCouriersTest extends BaseApiTestCase
{
  private string $sampleRequest;

  public function setUp(): void
  {
    parent::setUp();
    $this->sampleRequest = __DIR__ . '/../Fixtures/updateCouriers.json';
  }

  public function testFetchCouriersRequestCanBeProcessed()
  {
    $callback = function () {
      return ['Yodel International', 'TNT'];
    };
    $this->api->setRequestSource($this->sampleRequest);
    $response = $this->api->fetchCouriers($callback);

    $this->assertIsArray($response);
    $this->assertArrayHasKey('status', $response);
    $this->assertArrayHasKey('couriers', $response);
    $this->assertIsArray($response['couriers']);
  }

  public function testFetchCouriersExceptionIsHandled()
  {
    $callback = function () {
      throw new \Exception('invalid method', 444);
    };
    $this->api->setRequestSource($this->sampleRequest);
    $response = $this->api->fetchCouriers($callback);

    $this->assertIsArray($response);
    $this->assertArrayHasKey('error', $response);
    $this->assertArrayHasKey('success', $response);
    $this->assertArrayHasKey('status', $response);
    $this->assertSame(0, $response['success']);
    $this->assertSame(1, $response['error']);
    $this->assertSame('444', $response['status']);
  }

  public function testUpdateCouriersRequestCanBeProcessed()
  {
    $callback = function ($request) {
      $this->validateRequestParameters($request);

      return true;
    };
    $this->api->setRequestSource($this->sampleRequest);
    $response = $this->api->updateCouriers($callback);

    $this->validateSuccessResponse($response);
  }

  private function validateRequestParameters(UpdateCourierRequest $request)
  {
    $this->assertSame(Methods::UPDATE_COURIERS, $request->getMethod());
    $this->assertArrayHasKey('couriers', $request->getContent());
    $this->assertEmpty($request->getCouriers());
  }
}
