<?php

namespace Tests\Feature;

use Tests\BaseApiTestCase;

class GetSoftSearchKeyTest extends BaseApiTestCase
{
  public function testThatGetSoftSearchRequestReturnsSuccessfully()
  {
    $postParams = [
      'order_total' => 123.45,
      'retailer_base_url' => 'https://v5magento2.dbuytest.info',
    ];
    $response = $this->api->getSoftSearchKey($postParams);

    $this->assertIsArray($response);
    $this->assertArrayHasKey('status', $response);
    $this->assertArrayHasKey('splashKey', $response);
    $this->assertSame('ok', $response['status']);
  }
}
