<?php

namespace Tests\Feature;

use DivideBuySdk\Constant\Methods;
use DivideBuySdk\Request\Incoming\CancelOrderRequest;
use Tests\BaseApiTestCase;

class CancelOrderTest extends BaseApiTestCase
{
  private string $sampleRequest;

  public function setUp(): void
  {
    parent::setUp();
    $this->sampleRequest = __DIR__ . '/../Fixtures/cancelOrder.json';
  }

  public function testThatRequestMethodCanBeObtained()
  {
    $this->api->setRequestSource($this->sampleRequest);
    $method = $this->api->getIncomingRequestMethod();
    $this->assertNotEmpty($method);
    $this->assertSame('orderCancel', $method);
  }

  public function testCancelOrderRequestCanBeProcessed()
  {
    $callback = function ($request) {
      $this->validateRequestParameters($request);

      return true;
    };
    $this->api->setRequestSource($this->sampleRequest);
    $response = $this->api->cancelOrder($callback);

    $this->assertIsArray($response);
    $this->assertArrayHasKey('status', $response);
    $this->assertArrayHasKey('order_id', $response);
    $this->assertArrayHasKey('success', $response);
    $this->assertArrayHasKey('error', $response);
    $this->assertSame('ok', $response['status']);
    $this->assertSame(6667, $response['order_id']);
    $this->assertSame(0, $response['error']);
    $this->assertSame(1, $response['success']);
    $this->assertSame('orderCancel', $this->api->getIncomingRequestMethod());
  }

  private function validateRequestParameters(CancelOrderRequest $request)
  {
    $this->assertSame('default', $request->getRetailerStoreCode());
    $this->assertSame(6667, $request->getStoreOrderId());
    $this->assertSame(Methods::CANCEL_ORDER, $request->getMethod());
    $this->assertSame(true, $request->getDeleteUserOrder());
  }
}
