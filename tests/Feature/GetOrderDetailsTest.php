<?php

namespace Tests\Feature;

use DivideBuySdk\Data\RequestPayload;
use DivideBuySdk\Exception\InvalidParameterException;
use DivideBuySdk\Exception\MissingParameterException;
use DivideBuySdk\Request\Incoming\GetOrderDetailsRequest;
use DivideBuySdk\Response\Outgoing\GetOrderDetailsResponse;
use Tests\BaseApiTestCase;

class GetOrderDetailsTest extends BaseApiTestCase
{
  public function testThatGetOrderDetailsReturnsSuccessfully()
  {
    $json = @file_get_contents(__DIR__ . '/../Fixtures/Response/orderDetailsResponse.json');
    $orderDetails = json_decode($json, true);
    $callBack = function ($request) use ($orderDetails) {
      $this->assertRequestBody($request);

      return $orderDetails;
    };
    $this->api->setRequestSource(__DIR__ . '/../Fixtures/getOrderDetails.json');
    $response = $this->api->getOrderDetails($callBack);
    $nomalised = [
            'billing_address' =>
                [
                    'city' => 'Staffordshire',
                    'email' => 'retailer@dividebuy.co.uk',
                    'first_name' => 'First Name',
                    'last_name' => 'Last Name',
                    'postcode' => 'AA1 1AA',
                    'region' => 'Staffordshire',
                    'street' =>
                        [
                            'Brindley Court',
                            'Lymedale Business Park',
                        ],
                ],
            'order_detail' =>
                [
                    'discount' => 0,
                    'discountApplied' => 'beforeVat',
                    'discount_applied' => 'beforeVat',
                    'grand_total' => 318.4,
                    'is_default_billing' => 0,
                    'is_default_shipping' => 0,
                    'logo_url' => 'https://moduleinstalledmagento1.dbuytest.info/media/dividebuy/',
                    'selected_instalment' => 3,
                    'shipping' => 0.0,
                    'shippingInclVat' => 5.99,
                    'shipping_incl_vat' => 5.99,
                    'shipping_label' => 'Free Shipping - Free',
                    'shipping_method' => 'freeshipping_freeshipping',
                    'store_authentication' => '3aa7Sgt76sz7',
                    'store_order_id' => '6667',
                    'store_order_increment_id' => '145006485',
                    'store_token' => '5LIH1TaW8ewd',
                    'subtotal' => 265.33,
                    'subtotalInclVat' => 318.4,
                    'subtotal_incl_vat' => 318.4,
                    'vat' => 53.07,
                ],
            'product_details' =>
                [
                    [
                        'discount' => '0.0000',
                        'divVat' => '20',
                        'div_vat' => '20',
                        'image_url' => 'http://retailer.dev:8081/media//dividebuy/product/images//w/b/wbk012t.jpg',
                        'name' => 'Elizabeth Knit Top',
                        'price' => '210.0000',
                        'priceInclVat' => '231.0000',
                        'price_incl_vat' => '231.0000',
                        'product_options' =>
                            [
                                'Color' => 'Red',
                                'Size' => 'S',
                                'color' => 'Red',
                                'size' => 'S',
                            ],
                        'product_type' => 'configurable',
                        'product_visibility' => '4',
                        'product_weight' => null,
                        'qty' => '1.0000',
                        'rowTotal' => '210.0000',
                        'rowTotalInclVat' => '231.0000',
                        'row_total' => '210.0000',
                        'row_total_incl_vat' => '231.0000',
                        'short_description' => 'Description',
                        'sku' => 'wbk012c-Red-S',
                    ],
                    [
                        'discount' => '0.0000',
                        'divVat' => '20',
                        'div_vat' => '20',
                        'image_url' => 'http://retailer.dev:8081/media//dividebuy/product/images//w/b/wbk012t.jpg',
                        'name' => 'Elizabeth Knit Top',
                        'price' => '210.0000',
                        'priceInclVat' => '231.0000',
                        'price_incl_vat' => '231.0000',
                        'product_options' =>
                            [
                                'Color' => 'Red',
                                'Size' => 'S',
                                'color' => 'Red',
                                'size' => 'S',
                            ],
                        'product_type' => 'configurable',
                        'product_visibility' => '4',
                        'product_weight' => null,
                        'qty' => '1.0000',
                        'rowTotal' => '210.0000',
                        'rowTotalInclVat' => '231.0000',
                        'row_total' => '210.0000',
                        'row_total_incl_vat' => '231.0000',
                        'short_description' => 'Description',
                        'sku' => 'wbk012c-Red-S',
                    ],
                ],
            'shipping_address' =>
                [
                    'city' => 'Staffordshire',
                    'email' => 'retailer@dividebuy.co.uk',
                    'first_name' => 'First Name',
                    'last_name' => 'Last Name',
                    'postcode' => 'AA1 1AA',
                    'region' => 'Staffordshire',
                    'street' =>
                        [
                            'Brindley Court',
                            'Lymedale Business Park',
                        ],
                ],
        ];
    $this->assertSame($nomalised, $response);
  }

  public function testThatFailureResponseIsReturnedWhenAnExceptionIsThrown()
  {
    $callBack = function () {
      throw new InvalidParameterException('failed', 999);
    };
    $this->api->setRequestSource(__DIR__ . '/../Fixtures/getOrderDetails.json');
    $response = $this->api->getOrderDetails($callBack);

    $this->assertIsArray($response);
    $this->assertArrayHasKey('status', $response);
    $this->assertArrayHasKey('message', $response);
    $this->assertSame('999', $response['status']);
    $this->assertSame('failed', $response['message']);
  }

  /**
   * commented out pending decision on ticket https://dividebuy.atlassian.net/browse/EX-146
   */
  public function ThatExceptionIsThrownWithMissingData()
  {
    $this->expectException(MissingParameterException::class);
    $this->expectExceptionMessage('order_detail is required');
    $this->expectExceptionCode(400);

    $response = new GetOrderDetailsResponse(new RequestPayload([]));
    $response->setOrderDetails([]);

    $response->toArray();
  }

  private function assertRequestBody(GetOrderDetailsRequest $request)
  {
    $this->assertInstanceOf(GetOrderDetailsRequest::class, $request);
    $this->assertSame(6667, $request->getOrderId());
    $this->assertSame('default', $request->getRetailerStoreCode());
  }
}
