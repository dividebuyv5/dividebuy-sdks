<?php

namespace Tests\Feature;

use Tests\BaseApiTestCase;

class ConfirmRetailerTest extends BaseApiTestCase
{
  public function testThatConfirmRetailerRequestReturnsSuccessfully()
  {
    $postParams = [
            'retailerStoreCode' => $this->getEnv('STORE_CODE'),
            'CallRetailer' => '1',
        ];
    $response = $this->api->confirmRetailer($postParams);

    $this->assertIsArray($response);
    $this->assertArrayHasKey('status', $response);
    $this->assertArrayHasKey('retailerId', $response);
    $this->assertSame('ok', $response['status']);
  }
}
