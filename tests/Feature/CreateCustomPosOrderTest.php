<?php

namespace Tests\Feature;

use DivideBuySdk\Data\OrderAddress;
use DivideBuySdk\Request\Incoming\ActivatePosSystemRequest;
use DivideBuySdk\Request\Incoming\CreateCustomPosOrderRequest;
use Tests\BaseApiTestCase;

class CreateCustomPosOrderTest extends BaseApiTestCase
{
  private string $sampleRequest;

  public function setUp(): void
  {
    parent::setUp();
    $this->sampleRequest = __DIR__ . '/../Fixtures/createPosOrder.json';
  }

  public function testCreateCustomPosOrderRequestCanBeProcessed()
  {
    $callback = function ($request) {
      $this->validateRequestParameters($request);

      return [
        'order_increment_id' => 123,
        'order_id' => 456,
      ];
    };
    $this->api->setRequestSource($this->sampleRequest);
    $response = $this->api->createCustomPosOrder($callback);

    $this->validateSuccessResponse($response);

    $this->assertArrayHasKey('order_increment_id', $response);
    $this->assertArrayHasKey('order_id', $response);
  }

  private function validateRequestParameters(CreateCustomPosOrderRequest $request)
  {
    $this->assertSame('test', $request->getFirstName());
    $this->assertSame('tester', $request->getLastName());
    $this->assertSame(123.45, $request->getOrderTotal());
    $this->assertSame('test@gmail.com', $request->getCustomerEmail());
    $this->assertSame('2012-10-01 10:23:09', $request->getOrderTime());

    $address = $request->getAddress();
    $this->assertInstanceOf(OrderAddress::class, $address);

    $this->assertSame('divide buy house', $address->getHouseName());
    $this->assertSame('street 1', $address->getStreet());
    $this->assertSame('city', $address->getCity());
    $this->assertSame('12345678900', $address->getContactNumber());
    $this->assertSame('99', $address->getHouseNumber());
    $this->assertSame('address 2', $address->getAddress2());
    $this->assertSame('CD1 CD2', $address->getPostCode());
    $this->assertSame('london', $address->getRegion());
    $this->assertSame('', $address->getPrefix());
    $this->assertSame('', $address->getTelephone());
    $this->assertSame('', $address->getEmail());
  }
}
