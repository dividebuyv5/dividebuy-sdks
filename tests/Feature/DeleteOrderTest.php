<?php

namespace Tests\Feature;

use DivideBuySdk\Constant\Methods;
use DivideBuySdk\Request\Incoming\DeleteOrderRequest;
use Tests\BaseApiTestCase;

class DeleteOrderTest extends BaseApiTestCase
{
  private string $sampleRequest;

  public function setUp(): void
  {
    parent::setUp();
    $this->sampleRequest = __DIR__ . '/../Fixtures/deleteOrder.json';
  }

  public function testDeleteOrderRequestCanBeProcessed()
  {
    $callback = function ($request) {
      $this->validateRequestParameters($request);

      return true;
    };
    $this->api->setRequestSource($this->sampleRequest);
    $response = $this->api->deleteOrder($callback);

    $this->validateSuccessResponse($response);
  }

  private function validateRequestParameters(DeleteOrderRequest $request)
  {
    $this->assertSame('default', $request->getRetailerStoreCode());
    $this->assertSame(6667, $request->getStoreOrderId());
    $this->assertSame(Methods::DELETE_ORDER, $request->getMethod());
    $this->assertSame(true, $request->getDeleteUserOrder());
  }
}
