<?php

namespace Tests\Feature;

use DivideBuySdk\Api\ApiFactory;
use DivideBuySdk\Service\DomainResolver;
use GuzzleHttp\Exception\GuzzleException;
use Tests\BaseApiTestCase;

class SyncRetailerOrderTest extends BaseApiTestCase
{
  /**
   * @throws GuzzleException
   */
  public function testThatRetailerOrderCanBeSyncedSuccessfully()
  {
    $this->api = (new ApiFactory())->create('5LIH1TaW8ewd', '3aa7Sgt76sz7', DomainResolver::ENV_LOCAL);
    $response = $this->api->syncRetailerOrder(6667);

    $this->assertIsArray($response);
    $this->assertArrayHasKey('status', $response);
    $this->assertSame('ok', $response['status']);
  }
}
