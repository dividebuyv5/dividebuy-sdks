<?php

namespace Tests\Feature;

use Tests\BaseApiTestCase;

class VerifyTokenPermissionTest extends BaseApiTestCase
{
  public function testThatGetSoftSearchRequestReturnsSuccessfully()
  {
    $postParams = [
      'token' => 'djVtYWdlbnRvMl9kaXZpZGVidXlzYW5kYm94X2NvX3VrOktIZkJkQ2ZESjhrMzpMU3o2UW04Qm1kYk86MTIzLjQ1',
    ];
    $response = $this->api->verifyTokenPermission($postParams);

    $this->assertIsArray($response);
    $this->assertArrayHasKey('success', $response);
    $this->assertArrayHasKey('error', $response);
    $this->assertArrayHasKey('message', $response);
    $this->assertSame('Unauthorized', $response['message']);
    $this->assertSame(0, $response['success']);
    $this->assertSame(1, $response['error']);
  }
}
