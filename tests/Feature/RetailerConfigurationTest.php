<?php

namespace Tests\Feature;

use DivideBuySdk\Api;
use DivideBuySdk\Api\ApiFactory;
use DivideBuySdk\Constant\Methods;
use DivideBuySdk\Data\ConfigOption;
use DivideBuySdk\Request\Incoming\RetailerConfigurationRequest;
use DivideBuySdk\Request\RequestFactory;
use DivideBuySdk\Service\DomainResolver;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class RetailerConfigurationTest extends TestCase
{
  private string $sampleRequest;

  private Api $api;

  public function setUp(): void
  {
    $this->sampleRequest = __DIR__ . '/../Fixtures/retailerConfiguration.json';
    $this->api = (new ApiFactory())->create('5LIH1TaW8ewd', '3aa7Sgt76sz7', DomainResolver::ENV_LOCAL);
  }

  public function testRetailerConfigurationRequestCanBeProcessed()
  {
    $callback = function ($request) {
      $this->validateRequestParameters($request);

      return true;
    };
    $this->api->setRequestSource($this->sampleRequest);
    $response = $this->api->retailerConfiguration($callback);
    $this->assertInstanceOf(RetailerConfigurationRequest::class, $this->api->getIncomingRequest());

    $this->assertIsArray($response);
    $this->assertArrayHasKey('status', $response);
    $this->assertArrayHasKey('error', $response);
    $this->assertArrayHasKey('success', $response);
    $this->assertSame('ok', $response['status']);
    $this->assertSame(0, $response['error']);
    $this->assertSame(1, $response['success']);
    $this->assertSame('retailerConfigurations', $this->api->getIncomingRequestMethod());
  }

  /**
   * @dataProvider apiCredentialsProvider
   * @param mixed $storeToken
   * @param mixed $storeAuth
   */
  public function testStoreCredentialsCanBeValidated($storeToken, $storeAuth)
  {
    $api = (new ApiFactory())->create($storeToken, $storeAuth, DomainResolver::ENV_LOCAL);
    $callback = function () {
      return true;
    };
    $api->setRequestSource($this->sampleRequest);
    $response = $api->retailerConfiguration($callback);

    $this->assertIsArray($response);
    $this->assertArrayHasKey('status', $response);
    $this->assertArrayHasKey('error', $response);
    $this->assertArrayHasKey('success', $response);
    $this->assertSame('401', $response['status']);
    $this->assertSame(1, $response['error']);
    $this->assertSame(0, $response['success']);
  }

  private function validateRequestParameters(RetailerConfigurationRequest $request)
  {
    $this->assertSame('retailer_store_code', $request->getRetailerStoreCode());
    $this->assertSame(150, $request->getRetailerId());
    $this->assertSame(Methods::RETAILER_CONFIGURATIONS, $request->getMethod());
    $this->assertIsString($request->getRawRequest());
    $this->assertSame('retailer_store_code', $request->getRetailerStoreCode());

    $configDetails = $request->getConfigurationDetails();
    $this->assertContainsOnly(ConfigOption::class, $configDetails);

    $this->assertSame('global', $configDetails[0]->type);
    $this->assertSame('taxClass', $configDetails[0]->key);
    $this->assertSame('10', $configDetails[0]->value);
    $this->assertSame('150', $configDetails[0]->retailer_id);
  }

  public function testThatExceptionIsThrowWithNonImplementedIncomingRequests()
  {
    $path = __DIR__ . '/../Fixtures/invalidRequest.json';
    $this->expectException(InvalidArgumentException::class);
    $this->expectExceptionMessage('Request method invalidRequestMethod is not implemented');
    (new RequestFactory())->createIncoming($path);
  }

  /**
   * @dataProvider invalidCredentialPathProvider
   */
  public function testThatExceptionWithInvalidCredentailsInRequest($path)
  {
    (new RequestFactory())->createIncoming($path);
    $callback = function () {
    };
    $this->api->setRequestSource($path);
    $response = $this->api->retailerConfiguration($callback);

    $this->assertIsArray($response);
    $this->assertArrayHasKey('status', $response);
    $this->assertArrayHasKey('error', $response);
    $this->assertArrayHasKey('success', $response);
    $this->assertSame('400', $response['status']);
    $this->assertSame(1, $response['error']);
    $this->assertSame(0, $response['success']);
  }

  public function testThatExceptionIsThrowWithNonImplementedOutgoingRequests()
  {
    $this->expectException(InvalidArgumentException::class);
    $this->expectExceptionMessage('Request endpoint invalid-outgoing is not implemented');
    (new RequestFactory())->createOutgoing('invalid-outgoing');
  }

  public function apiCredentialsProvider()
  {
    return [
            [null, null],
            ['', null],
            [null, ''],
            ['', ''],
            ['store-token', null],
            [null, 'store-auth'],
            ['store-token', 'store-auth'],
        ];
  }

  public function invalidCredentialPathProvider()
  {
    return [
            [__DIR__ . '/../Fixtures/invalidStoreAuth.json'],
            [__DIR__ . '/../Fixtures/invalidStoreToken.json'],
            [__DIR__ . '/../Fixtures/invalidStoreCredentials.json'],
        ];
  }
}
