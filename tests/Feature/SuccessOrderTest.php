<?php

namespace Tests\Feature;

use DivideBuySdk\Constant\Methods;
use DivideBuySdk\Data\OrderAddress;
use DivideBuySdk\Request\Incoming\OrderSuccessRequest;
use Tests\BaseApiTestCase;

class SuccessOrderTest extends BaseApiTestCase
{
  private string $sampleRequest;

  public function setUp(): void
  {
    parent::setUp();
    $this->sampleRequest = __DIR__ . '/../Fixtures/successOrder.json';
  }

  public function testSuccessOrderRequestCanBeProcessed()
  {
    $callback = function ($request) {
      $this->validateRequestParameters($request);

      return true;
    };
    $this->api->setRequestSource($this->sampleRequest);
    $response = $this->api->successOrder($callback);

    $this->assertIsArray($response);
    $this->assertArrayHasKey('status', $response);
    $this->assertArrayHasKey('order_id', $response);
    $this->assertArrayHasKey('message', $response);
    $this->assertSame('ok', $response['status']);
    $this->assertSame('6667', $response['order_id']);
    $this->assertSame('Order placed successfully', $response['message']);
    $this->assertSame('orderSuccess', $this->api->getIncomingRequestMethod());
  }

  private function validateRequestParameters(OrderSuccessRequest $request)
  {
    $this->assertFalse($request->isPhoneOrderEnabled());
    $this->assertSame('default', $request->getRetailerStoreCode());
    $this->assertSame(6667, $request->getStoreOrderId());
    $this->assertSame(Methods::ORDER_SUCCESS, $request->getMethod());
    $this->assertInstanceOf(OrderAddress::class, $request->getAddress());
    $this->assertInstanceOf(\DateTime::class, $request->getOrderTime());
    $this->assertSame('retailer@dividebuy.co.uk', $request->getCustomerEmail());
    $this->assertSame('10000000067', $request->getOrderReferenceId());
    $this->assertSame('success', $request->getOrderStatus());

    $addressFields = ['prefix', 'first_name', 'last_name', 'contact_number', 'house_name', 'house_number', 'street', 'address2', 'postcode', 'region', 'city'];
    $address = $request->getAddress();
    foreach ($addressFields as $field) {
      $this->assertArrayHasKey($field, $address);
    }
  }
}
