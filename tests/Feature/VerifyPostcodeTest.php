<?php

namespace Tests\Feature;

use DivideBuySdk\Exception\InvalidParameterException;
use DivideBuySdk\Request\Incoming\VerifyPostcodeRequest;
use Tests\BaseApiTestCase;

class VerifyPostcodeTest extends BaseApiTestCase
{
  private string $sampleRequest;

  public function setUp(): void
  {
    parent::setUp();
    $this->sampleRequest = __DIR__ . '/../Fixtures/verifyPostcode.json';
  }

  public function testVerifyPostcodeRequestCanBeProcessed()
  {
    $callback = function ($request) {
      $this->validateRequestParameters($request);

      return true;
    };
    $this->api->setRequestSource($this->sampleRequest);
    $response = $this->api->verifyPostcode($callback);

    $this->assertIsArray($response);
    $this->assertArrayHasKey('status', $response);
    $this->assertArrayHasKey('error', $response);
    $this->assertArrayHasKey('success', $response);
    $this->assertArrayHasKey('message', $response);
    $this->assertSame('200', $response['status']);
    $this->assertSame(0, $response['error']);
    $this->assertSame(1, $response['success']);
    $this->assertSame('ok', $response['message']);
  }

  public function testThatFailureResponseIsReturnedWhenAnExceptionIsThrown()
  {
    $callBack = function () {
      throw new InvalidParameterException('failed', 999);
    };
    $this->api->setRequestSource($this->sampleRequest);
    $response = $this->api->verifyPostcode($callBack);

    $this->assertIsArray($response);
    $this->assertArrayHasKey('status', $response);
    $this->assertArrayHasKey('message', $response);
    $this->assertSame('999', $response['status']);
    $this->assertSame('failed', $response['message']);
  }

  private function validateRequestParameters(VerifyPostcodeRequest $request)
  {
    $this->assertSame('default', $request->getRetailerStoreCode());
    $this->assertSame(6734, $request->getOrderId());
    $this->assertSame('AA11AA', $request->getUserPostcode());
  }
}
