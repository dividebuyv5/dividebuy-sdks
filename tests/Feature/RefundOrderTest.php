<?php

namespace Tests\Feature;

use DivideBuySdk\Exception\InvalidParameterException;
use DivideBuySdk\Exception\MissingParameterException;
use DivideBuySdk\Request\Outgoing\RefundRequest;
use GuzzleHttp\Exception\GuzzleException;
use Tests\BaseApiTestCase;

class RefundOrderTest extends BaseApiTestCase
{
  public function testThatMissingParameterThrowsException()
  {
    $this->expectException(MissingParameterException::class);
    $this->expectExceptionMessage('retailer: storeAuthentication is required');

    $json = file_get_contents(__DIR__ . '/../Fixtures/invalidRefundOrder.json');
    $params = json_decode($json, true);

    $this->api->refundOrder($params);
  }

  /**
   * @throws GuzzleException
   */
  public function testThatRefundRequestReturnOrderNotFound()
  {
    $json = file_get_contents(__DIR__ . '/../Fixtures/refundOrder.json');
    $params = json_decode($json, true);
    $response = $this->api->refundOrder($params);

    $this->assertInstanceOf(RefundRequest::class, $this->api->getOutgoingRequest());

    $this->assertIsArray($response);
    $this->assertArrayHasKey('error', $response);
    $this->assertSame('Order not found for retailer', $response['error']['message']);
    $this->assertSame(422, $response['error']['status_code']);
  }

  public function testThatExceptionIsThrownWithInvalidProduct()
  {
    $this->expectException(InvalidParameterException::class);
    $this->expectExceptionMessage('product cannot be empty');
    $postParams = [
            'product' => '',
        ];
    $this->api->refundOrder($postParams);
  }
}
