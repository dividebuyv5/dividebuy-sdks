<?php

namespace Tests\Feature;

use DivideBuySdk\Api\ApiFactory;
use DivideBuySdk\Helper\DivideBuyLogger;
use DivideBuySdk\Service\DomainResolver;
use Psr\Log\LoggerInterface;
use Tests\BaseApiTestCase;
use Tests\CustomLogger;

class ApiFactoryTest extends BaseApiTestCase
{
  public function setUp(): void
  {
    $logger = $this->prophesize(DivideBuyLogger::class);
    $this->api = (new ApiFactory())->create(
            $this->getEnv('STORE_TOKEN'),
            $this->getEnv('STORE_AUTH'),
            '',
            $logger->reveal()
        );
  }

  public function testThatDefaultEnvIsProduction()
  {
    $this->assertTrue($this->api->isProduction());
    $this->assertFalse($this->api->isStaging());
    $this->assertFalse($this->api->isLocal());
  }

  public function testThatEnvCanBeChangedToStaging()
  {
    $this->api->changeEnvironment(DomainResolver::ENV_STAGING);
    $this->assertFalse($this->api->isProduction());
    $this->assertTrue($this->api->isStaging());
    $this->assertFalse($this->api->isLocal());
  }

  public function testThatEnvCanBeChangedToLocal()
  {
    $this->api->changeEnvironment(DomainResolver::ENV_LOCAL);
    $this->assertFalse($this->api->isProduction());
    $this->assertFalse($this->api->isStaging());
    $this->assertTrue($this->api->isLocal());
  }

  public function testThatApiLoggerCanUpdated()
  {
    $this->assertInstanceOf(DivideBuyLogger::class, $this->api->getLogger());
    $this->api->setLogger(new CustomLogger());

    $this->assertInstanceOf(LoggerInterface::class, $this->api->getLogger());
    $this->assertInstanceOf(CustomLogger::class, $this->api->getLogger());
  }
}
