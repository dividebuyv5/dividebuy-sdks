<?php

namespace Tests\Feature;

use DivideBuySdk\Data\ResponsePayload;
use DivideBuySdk\Exception\InvalidRequestPayloadException;
use DivideBuySdk\Request\RequestFactory;
use DivideBuySdk\Response\CommonResponse;
use DivideBuySdk\Response\ResponseFactory;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;

class RequestFactoryTest extends TestCase
{
  public function testThatExceptionIsThrownWithInvalidRequestBody()
  {
    $factory = new RequestFactory();
    $this->expectException(InvalidRequestPayloadException::class);
    $this->expectExceptionCode(400);
    $this->expectExceptionMessage('Request payload is invalid');
    $filename = __DIR__.'/../Fixtures/emptyBody.json';
    $factory->createIncoming($filename);
  }

  public function testExceptionIsThrownWithInvalidEndpoint()
  {
    $this->expectException(\InvalidArgumentException::class);
    $this->expectExceptionMessage('Request endpoint [invalid] is not implemented');

    $filename = __DIR__.'/../Fixtures/createPosOrder.json';
    (new RequestFactory())->createFromEndpoint($filename, 'invalid');
  }

  public function testResponseToArrayFor500Errors()
  {
    $resp = new CommonResponse();
    $resp->setResult(new Response(500, [], json_encode(['status' => 'ok'])));

    $return = $resp->toArray();

    $this->assertArrayHasKey('status', $return);
    $this->assertSame(500, $return['status']);
  }

  public function testCreateDefaultResponse()
  {
    $resp = (new ResponseFactory())->getOutgoingResponse('not-set', new ResponsePayload([]));
    $this->assertInstanceOf(CommonResponse::class, $resp);
  }
}
