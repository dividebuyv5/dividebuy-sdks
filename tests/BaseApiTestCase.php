<?php

namespace Tests;

use DivideBuySdk\Api;
use DivideBuySdk\Api\ApiFactory;
use DivideBuySdk\Helper\ArrayUtilityTrait;
use DivideBuySdk\Service\DomainResolver;

class BaseApiTestCase extends TestCase
{
  use ArrayUtilityTrait;

  protected Api $api;

  public function setUp(): void
  {
    $this->api = (new ApiFactory())->create($this->getEnv('STORE_TOKEN'), $this->getEnv('STORE_AUTH'), DomainResolver::ENV_LOCAL);
  }

  public function testThatDomainResolverIsPresent()
  {
    $this->assertInstanceOf(DomainResolver::class, $this->api->getUrlResolver());
  }

  protected function validateSuccessResponse($response)
  {
    $this->assertIsArray($response);
    $this->assertArrayHasKey('status', $response);
    $this->assertArrayHasKey('success', $response);
    $this->assertArrayHasKey('error', $response);
    $this->assertSame('ok', $response['status']);
    $this->assertSame(0, $response['error']);
    $this->assertSame(1, $response['success']);
  }
}
