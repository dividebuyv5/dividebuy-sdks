<?php

namespace Tests\Unit\Service;

use DivideBuySdk\Service\DomainResolver;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class DomainResolverTest extends TestCase
{
  /**
   * @dataProvider environmentParameterProvider
   * @param $environment
   */
  public function testThatDomainCanBeRetrievedPerEnvironment($environment)
  {
    $options = DomainResolver::getDomainOptions();
    $logger = $this->prophesize(LoggerInterface::class);
    $resolver = new DomainResolver($logger->reveal(), $environment);

    $this->assertSame(
            $options[$environment][DomainResolver::DOMAIN_API],
            $resolver->getApiDomain()
        );
    $this->assertSame(
            $options[$environment][DomainResolver::DOMAIN_ORDER],
            $resolver->getOrderDomain()
        );
    $this->assertSame(
            $options[$environment][DomainResolver::DOMAIN_PORTAL],
            $resolver->getPortalDomain()
        );
    $this->assertSame(
            $options[$environment][DomainResolver::DOMAIN_SOFT_SEARCH],
            $resolver->getSoftSearchDomain()
        );
  }

  public function environmentParameterProvider()
  {
    return [
            [DomainResolver::ENV_STAGING],
            [DomainResolver::ENV_PRODUCTION],
        ];
  }
}
