<?php

namespace Tests\Unit\Service\Authentication;

use DivideBuySdk\Exception\InvalidParameterException;
use DivideBuySdk\Service\Authentication\Parameters;
use PHPUnit\Framework\TestCase;

class ParameterTest extends TestCase
{
  public function testThatExceptionIsThrownWithInvalidStoreToken()
  {
    $this->expectException(InvalidParameterException::class);
    $this->expectExceptionMessage('Store Token not found');
    $parameter = new Parameters('', 'store-auth');
    $parameter->isValid();
  }

  public function testThatExceptionIsThrownWithInvalidStoreAuth()
  {
    $this->expectException(InvalidParameterException::class);
    $this->expectExceptionMessage('Store Authentication not found');
    $parameter = new Parameters('store-token', '');
    $parameter->isValid();
  }

  public function testThatAuthParameterCanBeValidated()
  {
    $parameter = new Parameters('store-token', 'store-auth');
    $valid = $parameter->isValid();
    $this->assertTrue($valid);
  }
}
