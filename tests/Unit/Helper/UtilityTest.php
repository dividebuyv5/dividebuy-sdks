<?php

namespace Tests\Unit\Helper;

use ArrayObject;
use DivideBuySdk\Helper\ArrayUtilityTrait;
use PHPUnit\Framework\TestCase;

class UtilityTest extends TestCase
{
  private $helper;

  public function setUp(): void
  {
    $this->helper = new class() {
      use ArrayUtilityTrait;
    };
  }

  /**
   * @dataProvider arrayDataProvider
   * @param mixed $array
   * @param mixed $key
   * @param mixed $expected
   * @param mixed $default
   */
  public function testArrGet($array, $key, $expected, $default = null)
  {
    $result = $this->helper->get($array, $key, $default);

    $this->assertSame($expected, $result);
  }

  public function testStringToCamelCase()
  {
    $key = 'store_token';
    $this->helper::camel($key);
    $this->assertSame('storeToken', $this->helper::camel($key));
  }

  public function testStringToSnakeCase()
  {
    $key = 'storeAuthentication';
    $this->helper::snake($key);

    $this->assertSame('store_authentication', $this->helper::snake($key));
  }

  public function testStringToStudlyCase()
  {
    $key = 'Retailer Store Code';
    $this->helper::studly($key);

    $this->assertSame('RetailerStoreCode', $this->helper::studly($key));
  }

  public function arrayDataProvider(): array
  {
    return [
            [[], 'key', null],
            [['first' => ['second' => 3]], 'first.second', 3],
            [new ArrayObject(['first' => ['second' => 3]]), 'first.second', 3],
            [['first' => 9], 'first', 9],
            [['first' => 9], 'first.second', null],
            [['first' => 9], null, ['first' => 9]],
            [
                false,
                null,
                '12av',
                function () {
                  return '12av';
                },
            ],
        ];
  }

  /**
   * @param string $key
   * @param mixed $value
   * @param mixed $expected
   * @dataProvider envParameterProvider
   */
  public function testGetEnvVariables($key, $value, $expected)
  {
    putenv("$key=$value");
    $result = $this->helper->getEnv($key);
    $this->assertSame($expected, $result);
  }

  public function testGetEnvVariablesWithClosure()
  {
    $result = $this->helper->getEnv('not-found', function () {
      return 'closure';
    });
    $this->assertSame('closure', $result);
  }

  public function envParameterProvider()
  {
    return [
            ['test-true', 'true', true],
            ['test-true', '(true)', true],
            ['test-false', 'false', false],
            ['test-false', '(false)', false],
            ['test-null', 'null', null],
            ['test-null', '(null)', null],
            ['test-empty', '(empty)', ''],
            ['test-empty', 'empty', ''],
            ['test-empty', '', ''],
            ['test-quotes', '"is-matched"', 'is-matched'],
        ];
  }
}
